import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { AppRegistry, AsyncStorage, View } from 'react-native'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/lib/integration/react'

import stores from './app/configureStore'
import App from './app/index'

// import Relay from 'react-relay'
// import {
//   AppRegistry,
//   StyleSheet,
//   Text,
//   View,
//   Navigator
// } from 'react-native'
//
// import AuthenticationScreen from './app/components/AuthenticationScreen'
// import SignUpStep1Screen from './app/components/SignUpStep1Screen'
// import SignUpStep2Screen from './app/components/SignUpStep2Screen'
// import AuthentifiedScreens from './app/components/AuthentifiedScreens'
//
// import SignUpRoute from './app/routes/SignUp'

if (!__DEV__)
  console.disableYellowBox = true

export default class Aimbet extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ready: false
    }
  }

  componentDidMount() {
    // persistStore(
    //   store,
    //   () => this.setState({ ready: true })
    // )
  }

  _renderScene(route, navigator) {
    let globalNavigatorProps = { navigator }

    switch(route.ident) {
      case "AuthenticationScreen":
        return (
          <AuthenticationScreen
            { ...globalNavigatorProps }/>
        )
      case "SignUpStep1Screen":
        return (
          <SignUpStep1Screen
            { ...globalNavigatorProps }/>
        )
      case "SignUpStep2Screen":
        return (
          <SignUpStep2Screen
            { ...route.data }
            { ...globalNavigatorProps }/>
        )
        // return (
        //   <Relay.Renderer
        //     Container={ SignUpStep2Screen }
        //     queryConfig={ new SignUpRoute() }
        //     environment={ Relay.Store }/>
        // )
      case "AuthentifiedScreens":
        return (
          <AuthentifiedScreens
            { ...route.data }
            { ...globalNavigatorProps }/>
        )
      default:
        return (
          // Should normally return an Error component
          <AuthenticationScreen
            { ...globalNavigatorProps }/>
        )

    }
  }

  render() {
      return (
        <Provider store={ stores.store }>
          <PersistGate
            loading={<View></View>}
            onBeforeLift={() => {}}
            persistor={ stores.persistor }>
            <App />
          </PersistGate>
        </Provider>
        // <Navigator
        //   initialRoute={ {ident: "AuthenticationScreen"} }
        //   ref="appNavigator"
        //   style={ styles.navigatorStyle }
        //   renderScene={ this._renderScene }
        //   configureScene={ (route) => ({
        //     ...route.sceneConfig || Navigator.SceneConfigs.FloatFromRight
        //   }) }/>
      )
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
// });

// Relay.injectNetworkLayer(
//   new Relay.DefaultNetworkLayer('https://secure-sierra-71157.herokuapp.com/graphql')
// )

AppRegistry.registerComponent('Aimbet', () => Aimbet)
