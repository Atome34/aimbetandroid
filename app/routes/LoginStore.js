import Relay from 'react-relay'

export default class LoginRoute extends Relay.Route {
    static routeName = 'LoginRoute'

    static queries = {
	     account_store : (Component) => Relay.QL `
      	query {
      	    account_store {
      		      ${Component.getFragment('account_store')}
      	    }
      	}
       `
    }
}
