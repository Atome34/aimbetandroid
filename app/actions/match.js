import {
  MATCH_GETALL,
  MATCH_GETALL_SUCCESS,
  MATCH_GETALL_FAILURE,
  MATCH_SELECT,
  MATCH_REMOVE
} from '../constants/match'
import { baseUrl } from '../constants/api'

/** MATCH GETALL **/

export function getAllMatch(gameIds) {
  return (dispatch) => {
    console.warn('In getAllMatch() action')
    dispatch(getAll())
    fetch(`${baseUrl}/api/matchs`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      }
    })
    .then(res => res.json())
    .then(json => {
      // dispatch(getAllFailure(json.message))
      console.warn('GET ALL MATCH SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        dispatch(getAllFailure(json.message))
        return
      }
      dispatch(getAllSuccess(json.matchs))
    })
    .catch(err => {
      console.warn('GET ALL MATCH ERROR : ', err)
      dispatch(getAllFailure(err.message))
    })
  }
}

function getAll() {
  return { type: MATCH_GETALL }
}

function getAllSuccess(data) {
  return {
    type: MATCH_GETALL_SUCCESS,
    payload: data
  }
}

function getAllFailure(data) {
  return {
    type: MATCH_GETALL_FAILURE,
    payload: data
  }
}

/** MATCH SELECT **/

export function selectMatch(matchData) {
  return (dispatch) => {
    console.log('In selectMatch() action')
    dispatch({
      type: MATCH_SELECT,
      payload: matchData
    })
  }
}

/** MATCH REMOVE **/

export function removeMatch(matchId) {
  return (dispatch) => {
    console.warn('In removeMatch() action')
    dispatch({
      type: MATCH_REMOVE,
      payload: matchId
    })
  }
}
