import {
  TICKET_CREATE,
  TICKET_CREATE_SUCCESS,
  TICKET_CREATE_FAILURE,
  TICKET_GET,
  TICKET_GET_SUCCESS,
  TICKET_GET_FAILURE,
  TICKET_GETALL,
  TICKET_GETALL_SUCCESS,
  TICKET_GETALL_FAILURE,
  TICKET_REMOVE,
  TICKET_REMOVE_SUCCESS,
  TICKET_REMOVE_FAILURE
} from '../constants/ticket'
import { SCREEN_PROFILE } from '../constants/screen'
import { baseUrl } from '../constants/api'
import { navigateTo } from './navigation'

/** TICKET CREATE **/

export function createTicket({username, matchs}) {
  return (dispatch) => {
    console.log('In createTicket() action')
    dispatch(create())
    fetch(`${baseUrl}/api/create_ticket`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        matchs
      })
    })
    .then(res => res.json())
    .then(json => {
      console.warn('CREATE TICKET SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        dispatch(createFailure(json.message))
        return
      }
      dispatch(navigateTo(SCREEN_PROFILE))
    })
    .catch(err => {
      console.warn('CREATE TICKET ERROR : ', err)
      dispatch(createFailure(err.message))
    })
  }
}

function create() {
  return { type: TICKET_CREATE }
}

function createSuccess(data) {
  return {
    type: TICKET_CREATE_SUCCESS
  }
}

function createFailure(data) {
  return {
    type: TICKET_CREATE_FAILURE,
    payload: data
  }
}

/** TICKET GET **/

export function getTicket(ticketId) {
  return (dispatch) => {
    //REQUEST API
    console.log('In getTicket() action')
    dispatch(get())
    fetch(`${baseUrl}/`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(json => {
      dispatch(getFailure(json.message))
      // console.warn('GET TICKET SUCCESS : ', json)
      // if (json.status < 200 || json.status >= 400) {
      //   dispatch(getFailure(json.message))
      //   return
      // }
      // dispatch(getSuccess(json.data))
    })
    .catch(err => {
      console.warn('GET TICKET ERROR : ', err)
      dispatch(getFailure(err.message))
    })
  }
}

function get() {
  return { type: TICKET_GET }
}

function getSuccess(data) {
  return {
    type: TICKET_GET_SUCCESS,
    payload: data
  }
}

function getFailure(data) {
  return {
    type: TICKET_GET_FAILURE,
    payload: data
  }
}

/** TICKET GETALL **/

export function getAllTicket(username) {
  return (dispatch) => {
    console.log('In getAllTicket() action')
    dispatch(getAll())
    fetch(`${baseUrl}/api/personnal_tickets`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body : JSON.stringify({
        username
      })
    })
    .then(res => res.json())
    .then(json => {
      // dispatch(getAllFailure(json.message))
      console.warn('GET ALL TICKET SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        dispatch(getAllFailure(json.message))
        return
      }
      dispatch(getAllSuccess(json.ticket))
    })
    .catch(err => {
      console.warn('GET ALL TICKET ERROR : ', err)
      dispatch(getAllFailure(err.message))
    })
  }
}

function getAll() {
  return { type: TICKET_GETALL }
}

function getAllSuccess(data) {
  return {
    type: TICKET_GETALL_SUCCESS,
    payload: data
  }
}

function getAllFailure(data) {
  return {
    type: TICKET_GETALL_FAILURE,
    payload: data
  }
}

/** TICKET REMOVE **/

export function removeTicket() {
  return (dispatch) => {
    console.log('In removeUser() action')
    dispatch(remove())
    fetch(`${baseUrl}/`, {
      method: 'remove',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      }
    })
    .then(res => res.json())
    .then(json => {
      dispatch(removeFailure(json.message))
      // console.warn('REMOVE TICKET SUCCESS : ', json)
      // if (json.status < 200 || json.status >= 400) {
      //   dispatch(removeFailure(json.message))
      //   return
      // }
      // dispatch(removeSuccess())
    })
    .catch(err => {
      console.warn('remove TICKET ERROR : ', err)
      dispatch(removeFailure(err.message))
    })
  }
}

function remove() {
  return { type: TICKET_REMOVE }
}

function removeSuccess() {
  return { type: TICKET_REMOVE_SUCCESS }
}

function removeFailure(data) {
  return {
    type: TICKET_REMOVE_FAILURE,
    payload: data
  }
}
