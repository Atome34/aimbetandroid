import {
  USER_GET,
  USER_GET_SUCCESS,
  USER_GET_FAILURE,
  USER_GETALL,
  USER_GETALL_SUCCESS,
  USER_GETALL_FAILURE,
  USER_REMOVE,
  USER_REMOVE_SUCCESS,
  USER_REMOVE_FAILURE
} from '../constants/user'
import { logoutAuth } from './auth'
import { baseUrl } from '../constants/api'

/** USER GET **/

export function getUser(username) {
  return (dispatch) => {
    console.log('In getUser() action')
    dispatch(get())
    fetch(`${baseUrl}/api/profile/${username}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(json => {
      console.warn('GET USER SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        dispatch(getFailure(json.message))
        return
      }
      dispatch(getSuccess(json.user))
    })
    .catch(err => {
      console.warn('GET USER ERROR : ', err)
      dispatch(getFailure(err.message))
    })
  }
}

function get() {
  return { type: USER_GET }
}

function getSuccess(data) {
  return {
    type: USER_GET_SUCCESS,
    payload: data
  }
}

function getFailure(data) {
  return {
    type: USER_GET_FAILURE,
    payload: data
  }
}

/** USER GETALL **/

export function getAllUser() {
  return (dispatch) => {
    console.log('In getAllUser() action')
    dispatch(getAll())
    fetch(`${baseUrl}/`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      }
    })
    .then(res => res.json())
    .then(json => {
      dispatch(getAllFailure(json.message))
      // console.warn('GET ALL USER SUCCESS : ', json)
      // if (json.status_code < 200 || json.status_code >= 400) {
      //   dispatch(getAllFailure(json.message))
      //   return
      // }
      // dispatch(getAllSuccess(json.data))
    })
    .catch(err => {
      console.warn('GET ALL USER ERROR : ', err)
      dispatch(getAllFailure(err.message))
    })
  }
}

function getAll() {
  return { type: USER_GETALL }
}

function getAllSuccess(data) {
  return {
    type: USER_GETALL_SUCCESS,
    payload: data
  }
}

function getAllFailure(data) {
  return {
    type: USER_GETALL_FAILURE,
    payload: data
  }
}

/** USER REMOVE **/

export function removeUser() {
  return (dispatch) => {
    console.log('In removeUser() action')
    dispatch(remove())
    fetch(`${baseUrl}/`, {
      method: 'REMOVE',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      }
    })
    .then(res => res.json())
    .then(json => {
      dispatch(removeFailure(json.message))
      // console.warn('REMOVE USER SUCCESS : ', json)
      // if (json.status_code < 200 || json.status_code >= 400) {
      //   dispatch(removeFailure(json.message))
      //   return
      // }
      // dispatch(removeSuccess())
      // dispatch(logoutAuth())
      // dispatch(navigateTo(SCREEN_UNAUTHENTIFIED))
    })
    .catch(err => {
      console.warn('REMOVE USER ERROR : ', err)
      dispatch(removeFailure(err.message))
    })
  }
}

function remove() {
  return { type: USER_REMOVE }
}

function removeSuccess() {
  return { type: USER_REMOVE_SUCCESS }
}

function removeFailure(data) {
  return {
    type: USER_REMOVE_FAILURE,
    payload: data
  }
}
