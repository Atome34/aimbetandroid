import {
  VIDEOGAME_GETALL,
  VIDEOGAME_GETALL_SUCCESS,
  VIDEOGAME_GETALL_FAILURE
} from '../constants/videogame'
import { baseUrl } from '../constants/api'

/** VIDEOGAME GETALL **/

export function getAllVideogame() {
  return (dispatch) => {
    console.log('In getAllVideogame() action')
    dispatch(getAll())
    fetch(`${baseUrl}/`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      }
    })
    .then(res => res.json())
    .then(json => {
      dispatch(getAllFailure(json.message))
      // console.warn('GET ALL VIDEOGAME SUCCESS : ', json)
      // if (json.status < 200 || json.status >= 400) {
      //   dispatch(getAllFailure(json.message))
      //   return
      // }
      // dispatch(getAllSuccess(json.data))
    })
    .catch(err => {
      console.warn('GET ALL VIDEOGAME ERROR : ', err)
      dispatch(getAllFailure(err.message))
    })
  }
}

function getAll() {
  return { type: VIDEOGAME_GETALL }
}

function getAllSuccess(data) {
  return {
    type: VIDEOGAME_GETALL_SUCCESS,
    payload: data
  }
}

function getAllFailure(data) {
  return {
    type: VIDEOGAME_GETALL_FAILURE,
    payload: data
  }
}
