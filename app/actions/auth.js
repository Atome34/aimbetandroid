import qs from 'query-string'

import {
  AUTH_LOGIN,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILURE,
  AUTH_SIGNUP,
  AUTH_SIGNUP_SUCCESS,
  AUTH_SIGNUP_FAILURE,
  AUTH_LOGOUT,
  AUTH_LOGOUT_SUCCESS,
  AUTH_LOGOUT_FAILURE,
  AUTH_SETEMAIL,
  AUTH_RESET
} from '../constants/auth'
import {
  SCREEN_AUTHENTIFIED,
  SCREEN_UNAUTHENTIFIED
} from '../constants/screen'
import { baseUrl } from '../constants/api'
import { navigateTo } from './navigation'

/** AUTH LOGIN **/

export function loginAuth({ username, password }) {
  return (dispatch) => {
    //REQUEST API
    console.warn('In loginAuth() action')
    console.warn('USERNAME: ', username)
    console.warn('Password: ', password)
    dispatch(login())
    fetch(
      `${baseUrl}/auth/login`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' },
        body: qs.stringify({ username, password })
      }
    )
    .then(res => res.json())
    .then(json => {
      console.warn('USER LOGIN SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        dispatch(loginFailure(json.message))
        return
      }
      dispatch(loginSuccess(json.user))
      dispatch(navigateTo(SCREEN_AUTHENTIFIED))
    })
    .catch(err => {
      console.warn('USER LOGIN ERROR : ', err)
      dispatch(loginFailure(err.message))
    })
  }
}

function login() {
  return { type: AUTH_LOGIN }
}

function loginSuccess(data) {
  return {
    type: AUTH_LOGIN_SUCCESS,
    payload: data
  }
}

function loginFailure(data) {
  return {
    type: AUTH_LOGIN_FAILURE,
    payload: data
  }
}

/** AUTH SIGNUP **/

export function signUpAuth({ email, username, password, avatar }) {
  return (dispatch) => {
    console.warn('In signUpAuth() action')
    dispatch(signUp())
    fetch(
      `${baseUrl}/auth/signup`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' },
        body: qs.stringify({ username, password })
      }
    )
    .then(res => res.json())
    .then(json => {
      // console.warn('USER SIGNUP SUCCESS : ', json)
      // dispatch(signUpFailure(json.message))
      console.warn('USER LOGIN SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        dispatch(signUpFailure(json.message))
        return
      }
      dispatch(signUpSuccess(json.user))
      dispatch(navigateTo(SCREEN_AUTHENTIFIED))
    })
    .catch(err => {
      console.warn('USER SIGNUP ERROR : ', err)
      dispatch(signUpFailure(err.message))
    })
  }
}

function signUp() {
  return { type: AUTH_LOGIN }
}

function signUpSuccess(data) {
  return {
    type: AUTH_SIGNUP_SUCCESS,
    payload: data
  }
}

function signUpFailure(data) {
  return {
    type: AUTH_SIGNUP_FAILURE,
    payload: data
  }
}

/** AUTH LOGOUT **/

export function logoutAuth() {
  return (dispatch) => {
    //REQUEST API
    console.warn('In logoutAuth() action')
    dispatch(logout())
    dispatch(logoutSuccess())
    dispatch(navigateTo(SCREEN_UNAUTHENTIFIED))
    // dispatch(logoutFailure())
  }
}

function logout() {
  return { type: AUTH_LOGOUT }
}

function logoutSuccess() {
  return {
    type: AUTH_LOGOUT_SUCCESS
  }
}

function logoutFailure(data) {
  return {
    type: AUTH_LOGOUT_FAILURE,
    payload: data
  }
}

/** AUTH RESET **/

export function resetAuth() {
  return (dispatch) => {
    console.warn('In resetAuth() action')
    dispatch({ type: AUTH_RESET })
  }
}

/** AUTH SETEMAIL **/

export function setEmailAuth(email) {
  return (dispatch) => {
    console.log('In setEmailAuth() action')
    dispatch(setEmail(email))
  }
}

function setEmail(email) {
  return {
    type: AUTH_SETEMAIL,
    payload: email
  }
}
