import React from 'react'
import { connect } from 'react-redux'
import { addNavigationHelpers, StackNavigator } from 'react-navigation'

import AuthenticationScreen from '../components/AuthenticationScreen'
import { SignUpNavigator } from './SignUpNavigator'
import {
  SCREEN_SIGNIN,
  SCREEN_SIGNUP
} from '../constants/screen'

export const UnauthentifiedNavigator = StackNavigator(
    {
      SCREEN_SIGNIN: { screen: AuthenticationScreen },
      SCREEN_SIGNUP: { screen: SignUpNavigator }
    },
    {
      // initialRouteName: SCREEN_SIGNIN,
      navigationOptions: {
        gesturesEnabled: false
      },
      headerMode: 'none',
      cardStyle: { backgroundColor: 'white'}
    }
)

const UnauthentifiedWithNavigationState = ({ dispatch, navigation }) => {
  return <UnauthentifiedNavigator navigation={ addNavigationHelpers({ dispatch, state: navigation }) } />
}

const mapStateToProps = state => ({
  navigation: state.unauthentifiedNavigation
})

export default connect(mapStateToProps)(UnauthentifiedWithNavigationState)
