import React from 'react'
import { connect } from 'react-redux'
import { addNavigationHelpers, TabNavigator } from 'react-navigation'

import RankingScreen from '../components/RankingScreen'
import CreateTicketScreen from '../components/CreateTicketScreen'
import ProfileScreen from '../components/ProfileScreen'

import {
  SCREEN_RANKING,
  SCREEN_TICKET_CREATION,
  SCREEN_PROFILE
} from '../constants/screen'

export const AuthentifiedNavigator = TabNavigator(
    {
      SCREEN_RANKING: { screen: RankingScreen },
      SCREEN_TICKET_CREATION: { screen: CreateTicketScreen },
      SCREEN_PROFILE: { screen: ProfileScreen }
    },
    {
      // initialRouteName: SCREEN_PROFILE,
      scrollEnabled: true,
      tabBarOptions: {
        activeTintColor:"#00B04D",
        inactiveTintColor:"white",
        style: {
          backgroundColor:'#0e1317'
        }
      }
    }
)

const AuthentifiedWithNavigationState = ({ dispatch, navigation }) => {
  return <AuthentifiedNavigator navigation={ addNavigationHelpers({ dispatch, state: navigation }) } />
}

const mapStateToProps = state => ({
  navigation: state.authentifiedNavigation
})

export default connect(mapStateToProps)(AuthentifiedWithNavigationState)
