import React from 'react'
import { connect } from 'react-redux'
import { addNavigationHelpers, StackNavigator } from 'react-navigation'

import SignUpStep1Screen from '../components/SignUpStep1Screen'
import SignUpStep2Screen from '../components/SignUpStep2Screen'
import {
  SCREEN_SIGNUP_STEP_1,
  SCREEN_SIGNUP_STEP_2
} from '../constants/screen'

export const SignUpNavigator = StackNavigator(
    {
      SCREEN_SIGNUP_STEP_1: { screen: SignUpStep1Screen },
      SCREEN_SIGNUP_STEP_2: { screen: SignUpStep2Screen }
    },
    {
      initialRouteName: SCREEN_SIGNUP_STEP_1,
      navigationOptions: {
        gesturesEnabled: true,
        gestureResponseDistance: {
          horizontal: 150
        }
      },
      headerMode: 'none',
      cardStyle: { backgroundColor: 'white'}
    }
)

const SignUpWithNavigationState = ({ dispatch, navigation }) => {
  return <SignUpNavigator navigation={ addNavigationHelpers({ dispatch, state: navigation }) } />
}

const mapStateToProps = state => ({
  navigation: state.signUpNavigation
})

export default connect(mapStateToProps)(SignUpWithNavigationState)
