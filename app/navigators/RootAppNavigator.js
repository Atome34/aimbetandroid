import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addNavigationHelpers, StackNavigator } from 'react-navigation'

import UnauthentifiedScreens from '../components/UnauthentifiedScreens'
import UnauthentifiedNavigator from './UnauthentifiedNavigator'
import AuthentifiedNavigator from './AuthentifiedNavigator'
import { SignUpNavigator } from '../navigators/SignUpNavigator'
import AuthentifiedScreens from '../components/AuthentifiedScreens'
import {
  SCREEN_AUTHENTIFIED,
  SCREEN_UNAUTHENTIFIED
} from '../constants/screen'

export const RootAppNavigator = StackNavigator(
    {
      SCREEN_UNAUTHENTIFIED: { screen: UnauthentifiedNavigator},
      SCREEN_AUTHENTIFIED: { screen: AuthentifiedNavigator }
    },
    {
      // initialRouteName: SCREEN_UNAUTHENTIFIED,
      navigationOptions: {
        gesturesEnabled: false
      },
      headerMode: 'none',
      cardStyle: { backgroundColor: 'white'}
    }
)



// const RootAppWithNavigationState = ({ dispatch, navigation, authentication }) => {
//   console.warn('USER IS: ', authentication.user)
//   const Root = RootAppNavigator(authentication.user)
//   return <Root navigation={ addNavigationHelpers({ dispatch, state: navigation }) } />
// }

class RootAppWithNavigationState extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: null
    }
  }

  componentDidMount() {
    const { authentication } = this.props
    console.warn('USER IS: ', authentication.user)
    this.setState({ user: authentication.user })
  }

  render() {
    const { dispatch, navigation, authentication } = this.props
    // const Root = RootAppNavigator()
    const state = this.state.user ? navigation.stateForLoggedIn : navigation.stateForLoggedOut
    return <RootAppNavigator navigation={ addNavigationHelpers({ dispatch, state: state }) }/>
  }
}

const mapStateToProps = state => ({
  navigation: state.rootNavigation,
  authentication: state.authentication
})

export default connect(mapStateToProps)(RootAppWithNavigationState)
