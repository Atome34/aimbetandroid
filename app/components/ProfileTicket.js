'use strict'

import React, { Component } from 'react'
import {
  StyleSheet,
  ScrollView,
  ListView,
  View,
  Text,
  Image,
  TouchableHighlight
} from 'react-native'

import ViewContainer from './ViewContainer'
import StatusBarBackground from './StatusBarBackground'
import ProfileTicketMatch from './ProfileTicketMatch'

class ProfileTicket extends Component {
  constructor(props) {
      super(props)
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
      this.state = {
        dataSource: ds.cloneWithRows([])
        // dataSource: ds.cloneWithRows(
        //   [
        //     {
        //       game: {
        //         name: 'CSGO',
        //         background: '../ressources/img/csgo_background.jpg'
        //       },
        //       teamA: {
        //         name: 'aAa',
        //         logo: require('../ressources/img/aaa_logo.png'),
        //         vote: 1345,
        //         selected: true
        //       },
        //       teamB: {
        //         name: 'Exes Esport',
        //         logo: require('../ressources/img/exes_logo.png'),
        //         vote: 564,
        //         selected: false
        //       }
        //     },
        //     {
        //       game: {
        //         name: 'OVERWATCH',
        //         background: '../ressources/img/overwatch_background.jpg'
        //       },
        //       teamA: {
        //         name: 'Team Curse',
        //         logo: require('../ressources/img/curse_logo.png'),
        //         vote: 4501,
        //         selected: false
        //       },
        //       teamB: {
        //         name: 'Fnatic',
        //         logo: require('../ressources/img/fnatic_logo.png'),
        //         vote: 32456,
        //         selected: true
        //       }
        //     },
        //     {
        //       game: {
        //         name: 'LOL',
        //         background: '../ressources/img/lol_background.jpg'
        //       },
        //       teamA: {
        //         name: 'Manalight',
        //         logo: require('../ressources/img/manalight_logo.png'),
        //         vote: 213,
        //         selected: true
        //       },
        //       teamB: {
        //         name: 'Tricked Esport',
        //         logo: require('../ressources/img/tricked_logo.png'),
        //         vote: 56,
        //         selected: false
        //       }
        //     }
        //   ]
        //)
      }
  }

  componentDidMount() {
    // this.setState({ dataSource: this.state.dataSource.cloneWithRows(this.props.matchs) })
  }

  render() {
    const ticket = this.props.ticket
    return (
      <View style={ styles.profileTicketWrapper }>
        <View style={ styles.profileTicket }>
          {/* <View style={ styles.profileTicketHeader }>
            <Text style={ [styles.profileTicketHeaderItem, { backgroundColor: '#F68D23' }] }>3/5 MATCHS EN COURS</Text>
            <Text style={ [styles.profileTicketHeaderItem, { backgroundColor: '#00B04D' }] }>FAIT LE 20/06/17</Text>
          </View> */}
          {/* <ListView
            style={ styles.profileTicketMatchList }
            enableEmptySections
            dataSource={ this.state.dataSource }
            renderRow={(rowData) => <ProfileTicketMatch match={ rowData }/>} /> */}
            <ScrollView
              style={ styles.profileTicketMatchList }
              showsVerticalScrollIndicator={ false }>
              {
                ticket.matchs.map((match, index) => (
                  <ProfileTicketMatch key={ index } match={ match }/>
                ))
              }
            </ScrollView>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({

  profileTicketWrapper: {
    flex:1,
    paddingLeft:25,
    paddingRight:25,
    flexDirection: 'column',
    justifyContent: 'center'
  },

  profileTicket: {
    flex:1
  },

  profileTicketHeader: {
    height:30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  profileTicketHeaderItem: {
    padding:5,
    fontSize:12,
    fontWeight: "bold",
    color:'white',
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  },

  profileTicketMatchList: {

  }

})

module.exports = ProfileTicket
