'use strict'

import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableHighlight } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import ViewContainer from './ViewContainer'
import StatusBarBackground from './StatusBarBackground'

class CreateTicketPickerWinner extends Component {

  constructor(props) {
    super(props)
    this.state = {}

    this.getColorVote = this.getColorVote.bind(this)
    this.getGameBackground = this.getGameBackground.bind(this)
    this.checkTeamSelected = this.checkTeamSelected.bind(this)
    this.selectWinner = this.selectWinner.bind(this)
  }

  getColorVote(team) {
    if (this.props.match.team_a.vote > this.props.match.team_b.vote)
      return team === "TEAM_A" ? '#00B04D' : '#D51515'
    else if (this.props.match.team_a.vote < this.props.match.team_b.vote)
      return team === "TEAM_A" ? '#D51515' : '#00B04D'
    return 'grey'
  }

  getGameBackground(name) {
    switch (name) {
      case 'CSGO':
        return require('../ressources/img/csgo_background.jpg')
      case 'OVERWATCH':
        return require('../ressources/img/overwatch_background.jpg')
      case 'LOL':
        return require('../ressources/img/lol_background.jpg')
      default:
        return null
    }
  }

  selectWinner(match, winnerId) {
    let winnerData = { match: match, winnerId: winnerId }
    this.props.selectWinner(winnerData)
  }

  checkTeamSelected(idMatch, idTeam) {
    const { teamsSelected } = this.props
    let matchIndex = teamsSelected.findIndex(matchItem => matchItem.match.match_id === idMatch)
    let isSelected = teamsSelected.findIndex(matchItem => matchItem.winnerId === idTeam)
    if (matchIndex >= 0 && isSelected >= 0)
      return true
    return false
  }

  render() {
    const match = this.props.match
    return (
          <View style={ styles.profileTicketMatchPicker }>
            <Image
              source={ this.getGameBackground('CSGO') }
              style={ styles.profileTicketMatch }/>
            <View style={ styles.profileTicketMatchWrapper }>
              <View style={ styles.profileTicketMatchTeam }>
                <View style={ [styles.profileTicketMatchTeamInfo, { justifyContent: 'flex-start' }] }>
                  <TouchableHighlight
                    style={ {justifyContent:'center'} }
                    onPress={ () => this.selectWinner(match, match.team_a.id) }>
                    <View style={ {flex:1} }>
                      <Image
                        source={ {uri: match.team_a.logo} }
                        style={ styles.profileTicketMatchTeamInfoAvatar }/>
                      <View style={ styles.profileTicketMatchTeamInfoAvatarPickerWrapper }>
                        <View style={ styles.profileTicketMatchTeamInfoAvatarPicker }>
                          {
                            this.checkTeamSelected(match.match_id, match.team_a.id) ? (
                              <Icon name="trophy" size={17} color="#f4d244" />
                            ) : null
                          }
                        </View>
                      </View>
                    </View>
                  </TouchableHighlight>
                  {/* <View style={ styles.profileTicketMatchTeamInfoStats }>
                    <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                      <Text style={ styles.profileTicketMatchTeamInfoStatsText }>VOTE</Text>
                      <Text
                        style={ [
                          styles.profileTicketMatchTeamInfoStatsText,
                          styles.profileTicketMatchTeamInfoStatsTextValue
                        ] }>{ this.props.teamsSelected.length }</Text>
                    </View>
                    <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                      <Text style={ styles.profileTicketMatchTeamInfoStatsText }>COTE</Text>
                      <Text style={ [
                        styles.profileTicketMatchTeamInfoStatsText,
                        styles.profileTicketMatchTeamInfoStatsTextValue,
                        { backgroundColor: '#F68D23'}
                      ] }>%</Text>
                    </View>
                  </View> */}
                  <View style={ styles.profileTicketMatchTeamInfoStats }>
                    <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                      <Text style={ styles.profileTicketMatchTeamInfoStatsText }>VOTE</Text>
                      <Text
                        style={ [
                          styles.profileTicketMatchTeamInfoStatsText,
                          styles.profileTicketMatchTeamInfoStatsTextValue,
                          { backgroundColor: this.getColorVote('TEAM_A') }
                        ] }>{ match.team_a.vote }</Text>
                    </View>
                    <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                      <Text style={ styles.profileTicketMatchTeamInfoStatsText }>COTE</Text>
                      <Text style={ [
                        styles.profileTicketMatchTeamInfoStatsText,
                        styles.profileTicketMatchTeamInfoStatsTextValue,
                        { backgroundColor: '#F68D23'}
                      ] }>{ (match.team_a.vote * 100 / (match.team_a.vote + match.team_b.vote)).toFixed(1) }%</Text>
                    </View>
                  </View>
                </View>
                <View style={ {flexDirection: 'row'} }>
                  <Text style={ [styles.profileTicketMatchTeamName, { backgroundColor: this.checkTeamSelected(match.match_id, match.team_a.id) ? '#00B04D' : 'rgba(0, 0, 0, 0.5)' }] }>
                    { match.team_a.name.toUpperCase() }
                  </Text>
                </View>
              </View>

              <View style={ styles.profileTicketMatchTeam }>
                <View style={ [styles.profileTicketMatchTeamInfo, { justifyContent: 'flex-end' }] }>
                  <View style={ styles.profileTicketMatchTeamInfoStats }>
                    <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                      <Text style={ styles.profileTicketMatchTeamInfoStatsText }>VOTE</Text>
                      <Text style={ [
                        styles.profileTicketMatchTeamInfoStatsText,
                        styles.profileTicketMatchTeamInfoStatsTextValue,
                        { backgroundColor: this.getColorVote('TEAM_B') }
                      ] }>{ match.team_b.vote }</Text>
                    </View>
                    <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                      <Text style={ styles.profileTicketMatchTeamInfoStatsText }>COTE</Text>
                      <Text style={ [
                        styles.profileTicketMatchTeamInfoStatsText,
                        styles.profileTicketMatchTeamInfoStatsTextValue,
                        { backgroundColor: '#F68D23'}]
                      }>{ (match.team_b.vote * 100 / (match.team_a.vote + match.team_b.vote)).toFixed(1) }%</Text>
                    </View>
                  </View>
                  <TouchableHighlight
                    onPress={ () => this.selectWinner(match, match.team_b.id) }>
                    <View style={ {justifyContent:'center'} }>
                      <Image
                        source={ {uri: match.team_b.logo} }
                        style={ styles.profileTicketMatchTeamInfoAvatar }/>
                      <View style={ styles.profileTicketMatchTeamInfoAvatarPickerWrapper }>
                        <View style={ styles.profileTicketMatchTeamInfoAvatarPicker }>
                          {
                            this.checkTeamSelected(match.match_id, match.team_b.id) ? (
                              <Icon name="trophy" size={17} color="#f4d244" />
                            ) : null
                          }
                        </View>
                      </View>
                    </View>
                  </TouchableHighlight>
                </View>
                <View style={ {flexDirection: 'row', justifyContent: 'flex-end'} }>
                  <Text style={ [styles.profileTicketMatchTeamName, { backgroundColor: this.checkTeamSelected(match.match_id, match.team_b.id) ? '#00B04D' : 'rgba(0, 0, 0, 0.5)' }] }>
                    { match.team_b.name.toUpperCase() }
                  </Text>
                </View>
              </View>
            </View>
          </View>
    )
  }
}

const styles = StyleSheet.create({

  profileTicketMatchPicker: {
    flex:1,
    height:110,
    marginBottom:10,
    flexDirection:'column',
    alignItems:'stretch',
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  },

  profileTicketMatch: {
    flex:1,
    width:null,
    height:null,
    resizeMode:'cover'
  },

  profileTicketMatchPickerHeader: {
    paddingTop:3,
    paddingBottom:3,
    paddingLeft:10,
    paddingRight:10,
    textAlign: 'center',
    color:'black',
    fontWeight: 'bold',
    backgroundColor:'white'
  },

  profileTicketMatchWrapper: {
    flex:1,
    justifyContent: 'space-between',
    alignSelf:'stretch',
    left:0,
    right:0,
    flexDirection: 'row',
    position:'absolute',
    backgroundColor: 'rgba(0 ,0, 0, 0.3)'
  },

  profileTicketMatchTeam: {
    height:110,
    justifyContent:'space-between'
  },

  profileTicketMatchTeamInfo: {
    flexDirection: 'row',
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4
    }
  },

  profileTicketMatchTeamInfoAvatar: {
    width:80,
    height:80,
    backgroundColor: 'white',
    resizeMode: 'contain',
    alignItems: 'stretch'
  },

  profileTicketMatchTeamInfoAvatarPickerWrapper: {
    flex:1,
    position:'absolute',
    left:0,
    width:80,
    height:80,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent:'center',
    alignSelf:'stretch',
    alignItems: 'center'
  },

  profileTicketMatchTeamInfoAvatarPicker: {
    width:35,
    height:35,
    borderWidth:3,
    borderColor: 'white',
    alignItems: 'center',
    justifyContent:'center'
  },

  profileTicketMatchTeamInfoStats: {
    height:80,
    paddingLeft:10,
    paddingRight:10,
    paddingTop:5,
    paddingBottom:5,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'space-between'
  },

  profileTicketMatchTeamInfoStatsText: {
      color:'white',
      fontWeight: 'bold',
      fontSize:12,
      textAlign: 'center'
  },

  profileTicketMatchTeamInfoStatsTextValue: {
    paddingLeft:5,
    paddingRight:5,
    paddingTop:3,
    paddingBottom:3
  },

  profileTicketMatchTeamName: {
    backgroundColor:'rgba(0, 0, 0, 0.5)',
    color:'white',
    padding:5,
    fontWeight: 'bold',
    marginTop:5,
    fontSize:12
  }

})

module.exports = CreateTicketPickerWinner
