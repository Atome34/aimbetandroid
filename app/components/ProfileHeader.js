'use strict'

import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableHighlight, Platform } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

const ProfileHeader = (props) => {
    return (
      <View style={ styles.profileHeader }>
        {
          props.profileStep !== 0 ? (
            <TouchableHighlight
              onPress={ () => props.setProfileStep(props.profileStep - 1) }
              underlayColor='transparent'>
              <Text style={ {color: 'white', fontWeight: 'bold'} }>Retour</Text>
            </TouchableHighlight>
          ) : <Text style={ {color: 'transparent', fontWeight: 'bold'} }>...</Text>
        }
        {
          props.profileStep === 0 ? (
            <Text style={ styles.profileHeaderTitle }>{ props.username }</Text>
          ) : <Text style={ styles.profileHeaderTitle }>Réglages</Text>
        }
        <TouchableHighlight
          onPress={ () => props.setProfileStep(props.profileStep + 1) }
          underlayColor='transparent'>
          {
            props.profileStep === 0 ? (
              <Icon name="cog" size={18} color="white" />
            ) : (
              <TouchableHighlight
                onPress={ () => props.setProfileStep(props.profileStep - 1) }
                underlayColor='transparent'>
                <Text style={ styles.profileHeaderTitle }>OK</Text>
              </TouchableHighlight>
            )
          }
        </TouchableHighlight>
      </View>
    )
}

const styles = StyleSheet.create({

  profileHeader: {
    height: 40,
    borderBottomWidth:0.5,
    borderColor: '#00B04D',
    backgroundColor: '#0e1317',
    paddingLeft:25,
    paddingRight:25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  profileHeaderTitle: {
    color: 'white',
    fontWeight: 'bold'
  }

})

module.exports = ProfileHeader
