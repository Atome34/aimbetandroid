'use strict'

import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  ListView,
  Image,
  TouchableHighlight,
  Platform
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'
import CreateTicketHeader from './CreateTicketHeader'

class CreateTicketSelectGames extends Component {
  constructor(props) {
      super(props)
      //const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
      this.state = {
        // games: games,
        // dataSource: ds.cloneWithRows(games)
      }

      this.displayGameRow = this.displayGameRow.bind(this)
      // this.selectGame = this.selectGame.bind(this)
  }

  displayGameRow(rowData) {
    return (
      <TouchableHighlight
        onPress={ () => this.props.selectVideogame(rowData.id) }
        underlayColor='transparent'
        style={ styles.createTicketGame }>
        <View style={ [styles.createTicketGameWrapper, {borderColor: rowData.selected ? '#00B04D' : 'transparent'}] }>
          <View style={ styles.createTicketGameLogoWrapper }>
            <Image
              source={ rowData.logo }
              style={ styles.createTicketGameLogo }/>
          </View>
          <Text style={ styles.createTicketGameName }>{ (rowData.name).toUpperCase() }</Text>
        </View>
      </TouchableHighlight>
    )
  }

  // selectGame(rowData) {
  //   let gamesSelected = this.state.gamesSelected.slice()
  //   let games = this.state.games.slice()
  //   if (rowData.selected === true) {
  //     for (let i = 0; i < gamesSelected.length; ++i)
  //       if (gamesSelected[i] == rowData.id)
  //         gamesSelected.splice(i, 1)
  //   }
  //   else
  //     gamesSelected.push(rowData.id)
  //   for (let i = 0; i < games.length; ++i) {
  //     if (games[i].id == rowData.id) {
  //       if (rowData.selected)
  //         games[i] = { ...this.state.games[i], selected: false }
  //       else
  //         games[i] = { ...this.state.games[i], selected: true }
  //     }
  //   }
  //   this.setState({
  //     gamesSelected: gamesSelected,
  //     games: games,
  //     dataSource: this.state.dataSource.cloneWithRows(games)
  //   })
  // }

  render() {
    return (
      <View style={ styles.createTicket }>
        <CreateTicketHeader
          setCreateTicketStep={ this.props.setCreateTicketStep }
          createTicketStep={ 0 }
          isNextStepReady={ this.props.videogamesSelected.length > 0 ? true : false }/>
        <ScrollView
          showsVerticalScrollIndicator={ false }
          contentContainerStyle={ styles.createTicketGamesList }>
          {
            this.props.videogames.map((videogame, index) => {
              const isSelected = this.props.videogamesSelected.findIndex(id => id == videogame.id)
              return (
                <TouchableHighlight
                  key={ index }
                  onPress={ () => this.props.selectVideogame(videogame.id) }
                  underlayColor='transparent'
                  style={ styles.createTicketGame }>
                  <View style={ [styles.createTicketGameWrapper, {borderColor: isSelected !== -1 ? '#00B04D' : 'transparent'}] }>
                    <View style={ styles.createTicketGameLogoWrapper }>
                      <Image
                        source={ videogame.logo }
                        style={ styles.createTicketGameLogo }/>
                    </View>
                    <Text style={ styles.createTicketGameName }>{ (videogame.name).toUpperCase() }</Text>
                  </View>
                </TouchableHighlight>
              )
            })
          }
        </ScrollView>
        {/* <ListView
          contentContainerStyle={ styles.createTicketGamesList }
          dataSource={this.props.dataSourceGames}
          renderRow={ (rowData) => this.displayGameRow(rowData) } /> */}
      </View>
    )
  }
}

const styles = StyleSheet.create({

  createTicket: {
    flex: 1
  },

  createTicketGamesList: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    paddingLeft: 25,
    paddingRight: 25
  },

  createTicketGame: {
    width:135,
    height:135,
    margin:10,
    flexDirection: 'column',
  },

  createTicketGameWrapper: {
    flex:1,
    padding:5,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    borderWidth: 1.5,
    borderColor: 'transparent'
  },

  createTicketGameLogoWrapper: {
    height:97.5,
    alignItems: 'center'
  },

  createTicketGameLogo: {
    flex:1,
    width:90,
    resizeMode: 'contain'
  },

  createTicketGameName: {
    fontSize:11,
    justifyContent:'center',
    textAlign: 'center',
    alignSelf: 'stretch',
    height:25,
    paddingTop: 6,
    paddingLeft: 10,
    paddingRight: 10,
    color:'white',
    fontWeight: 'bold',
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  }

})

module.exports = CreateTicketSelectGames
