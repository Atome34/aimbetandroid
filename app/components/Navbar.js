'use strict'

import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableHighlight, Platform } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const Navbar = (props) => {
    return (
        <View style={ styles.navbar }>
          {/* <TouchableHighlight
            style={ styles.navbarIcon }
            onPress={ () => props.setNav('HOME') }
            underlayColor='transparent'>
              <Icon name="home" size={21} color={ props.navSelected === 'HOME' ? '#00B04D' : 'white' } />
          </TouchableHighlight> */}
          <TouchableHighlight
            style={ styles.navbarIcon }
            onPress={ () => props.setNav('RANKING') }
            underlayColor='transparent'>
            <Icon name="trophy" size={21} color={ props.navSelected === 'RANKING' ? '#00B04D' : 'white' } />
          </TouchableHighlight>
          <TouchableHighlight
            style={ styles.navbarIcon }
            onPress={ () => props.setNav('CREATE_TICKET') }
            underlayColor='transparent'>
            <Icon name="ticket" size={21} color={ props.navSelected === 'CREATE_TICKET' ? '#00B04D' : 'white' } />
          </TouchableHighlight>
          {/* <TouchableHighlight
            style={ styles.navbarIcon }
            onPress={ () => props.setNav('NOTIFICATIONS') }
            underlayColor='transparent'>
            <Icon name="bell" size={21} color={ props.navSelected === 'NOTIFICATIONS' ? '#00B04D' : 'white' } />
          </TouchableHighlight> */}
          <TouchableHighlight
            style={ styles.navbarIcon }
            onPress={ () => props.setNav('PROFILE') }
            underlayColor='transparent'>
            <Icon name="user" size={21} color={ props.navSelected === 'PROFILE' ? '#00B04D' : 'white' } />
          </TouchableHighlight>
        </View>
    )
}

const styles = StyleSheet.create({

  navbar: {
    height: 47,
    borderTopWidth:0.5,
    borderColor: '#00B04D',
    backgroundColor: '#0e1317',
    paddingLeft:25,
    paddingRight:25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  navbarIcon: {
    padding:15
  }

})

module.exports = Navbar
