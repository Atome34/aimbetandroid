import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  ListView,
  ScrollView
} from 'react-native'

import CreateTicketHeader from './CreateTicketHeader'
import CreateTicketValidateWinner from './CreateTicketValidateWinner'

const CreateTicketValidateTicket = (props) => {
    return (
      <View style={ styles.createTicket }>
        <CreateTicketHeader
          setCreateTicketStep={ props.setCreateTicketStep }
          createTicketStep={ 2 }
          isNextStepReady={ props.teamsSelected.length > 0 ? true : false }/>
        <ScrollView
          showsVerticalScrollIndicator={ false }
          contentContainerStyle={ styles.createTicketGamesList }>
          {
            props.teamsSelected.map((match, index) => (
              <CreateTicketValidateWinner
                key={ index }
                teamsSelected={ props.teamsSelected }
                removeMatch={ props.removeMatch }
                match={ match }/>
            ))
          }
        </ScrollView>
      </View>
    )
}

const styles = StyleSheet.create({
  createTicket: {
    flex: 1
  },

  createTicketGamesList: {
    paddingHorizontal:10,
    paddingTop:10
  }
})

export default CreateTicketValidateTicket
