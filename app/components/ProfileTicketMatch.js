'use strict'

import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableHighlight
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import ViewContainer from './ViewContainer'
import StatusBarBackground from './StatusBarBackground'

class ProfileTicketMatch extends Component {
  constructor(props) {
      super(props)
      this.state = {}

      this.getGameBackground = this.getGameBackground.bind(this)
      this.getColorVote = this.getColorVote.bind(this)
  }

  getColorVote(team) {
    if (this.props.match.team_a.vote > this.props.match.team_b.vote)
      return team === "TEAM_A" ? '#00B04D' : '#D51515'
    else if (this.props.match.team_a.vote < this.props.match.team_b.vote)
      return team === "TEAM_A" ? '#D51515' : '#00B04D'
    return 'grey'
  }

  getGameBackground(name) {
    switch (name) {
      case 'CSGO':
        return require('../ressources/img/csgo_background.jpg')
      case 'OVERWATCH':
        return require('../ressources/img/overwatch_background.jpg')
      case 'LOL':
        return require('../ressources/img/lol_background.jpg')
      default:
        return null
    }
  }

  render() {
    return (
      <ImageBackground
        source={ this.getGameBackground('CSGO') }
        style={ styles.profileTicketMatch }>
        <View style={ styles.profileTicketMatchWrapper }>
          <View style={ styles.profileTicketMatchTeam }>
            <View style={ [styles.profileTicketMatchTeamInfo, { justifyContent: 'flex-start' }] }>
              <ImageBackground
                source={ {uri: this.props.match.team_a.logo} }
                style={ styles.profileTicketMatchTeamInfoAvatar }>
                {
                  this.props.match.prono_team != this.props.match.team_a.id ? (
                    <View style={ {flex:1, backgroundColor: 'rgba(0, 0, 0, 0.5)'} }></View>
                  ) : null
                }
              </ImageBackground>
              <View style={ styles.profileTicketMatchTeamInfoStats }>
                <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                  <Text style={ styles.profileTicketMatchTeamInfoStatsText }>VOTE</Text>
                  <Text
                    style={ [
                      styles.profileTicketMatchTeamInfoStatsText,
                      styles.profileTicketMatchTeamInfoStatsTextValue,
                      { backgroundColor: this.getColorVote('TEAM_A') }
                    ] }>{ this.props.match.team_a.vote }</Text>
                </View>
                <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                  <Text style={ styles.profileTicketMatchTeamInfoStatsText }>COTE</Text>
                  <Text style={ [
                    styles.profileTicketMatchTeamInfoStatsText,
                    styles.profileTicketMatchTeamInfoStatsTextValue,
                    { backgroundColor: '#F68D23'}
                  ] }>{ (this.props.match.team_a.vote * 100 / (this.props.match.team_a.vote + this.props.match.team_b.vote)).toFixed(1) }%</Text>
                </View>
              </View>
            </View>
            <View style={ {flexDirection: 'row'} }>
              <Text style={ [styles.profileTicketMatchTeamName, { backgroundColor: this.props.match.prono_team == this.props.match.team_a.id ? '#00B04D' : 'rgba(0, 0, 0, 0.5)' }] }>
                { (this.props.match.team_a.name).toUpperCase() }
              </Text>
            </View>
          </View>
          <View style={ styles.profileTicketMatchTeam }>
            <View style={ [styles.profileTicketMatchTeamInfo, { justifyContent: 'flex-end' }] }>
              <View style={ styles.profileTicketMatchTeamInfoStats }>
                <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                  <Text style={ styles.profileTicketMatchTeamInfoStatsText }>VOTE</Text>
                  <Text style={ [
                    styles.profileTicketMatchTeamInfoStatsText,
                    styles.profileTicketMatchTeamInfoStatsTextValue,
                    { backgroundColor: this.getColorVote('TEAM_B') }
                  ] }>{ this.props.match.team_b.vote }</Text>
                </View>
                <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                  <Text style={ styles.profileTicketMatchTeamInfoStatsText }>COTE</Text>
                  <Text style={ [
                    styles.profileTicketMatchTeamInfoStatsText,
                    styles.profileTicketMatchTeamInfoStatsTextValue,
                    { backgroundColor: '#F68D23'}]
                  }>{ (this.props.match.team_b.vote * 100 / (this.props.match.team_a.vote + this.props.match.team_b.vote)).toFixed(1) }%</Text>
                </View>
              </View>
              <ImageBackground
                source={ {uri: this.props.match.team_b.logo} }
                style={ styles.profileTicketMatchTeamInfoAvatar }>
                {
                  this.props.match.prono_team != this.props.match.team_b.id ? (
                    <View style={ {flex:1, backgroundColor: 'rgba(0, 0, 0, 0.5)'} }></View>
                  ) : null
                }
              </ImageBackground>
            </View>
            <View style={ {flexDirection: 'row', justifyContent: 'flex-end'} }>
              <Text style={ [styles.profileTicketMatchTeamName, { backgroundColor: this.props.match.prono_team == this.props.match.team_b.id ? '#00B04D' : 'rgba(0, 0, 0, 0.5)'  }] }>
                { (this.props.match.team_b.name).toUpperCase() }
              </Text>
            </View>
          </View>
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({

  profileTicketMatch: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    flex:1,
    width:null,
    height:null,
    marginBottom:10,
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  },

  profileTicketMatchWrapper: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    flex:1,
    backgroundColor: 'rgba(0 ,0, 0, 0.3)'
  },

  profileTicketMatchTeam: {
    flex:1
  },

  profileTicketMatchTeamInfo: {
    flexDirection: 'row',
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  },

  profileTicketMatchTeamInfoAvatar: {
    width:80,
    height:80,
    backgroundColor: 'white',
    resizeMode: 'contain'
  },

  profileTicketMatchTeamInfoStats: {
    height:80,
    paddingLeft:10,
    paddingRight:10,
    paddingTop:5,
    paddingBottom:5,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'space-between'
  },

  profileTicketMatchTeamInfoStatsText: {
      color:'white',
      fontWeight: 'bold',
      fontSize:12,
      textAlign: 'center'
  },

  profileTicketMatchTeamInfoStatsTextValue: {
    paddingLeft:5,
    paddingRight:5,
    paddingTop:3,
    paddingBottom:3
  },

  profileTicketMatchTeamName: {
    backgroundColor:'rgba(0, 0, 0, 0.5)',
    color:'white',
    padding:5,
    fontWeight: 'bold',
    marginTop:5,
    fontSize:12
  }

})

module.exports = ProfileTicketMatch
