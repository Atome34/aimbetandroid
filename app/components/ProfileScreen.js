'use strict'

import qs from 'qs'

import React, { Component } from 'react'
import Relay from 'react-relay'
import { connect } from 'react-redux'
import { AsyncStorage, StatusBar, StyleSheet, View, Text, Image, TouchableHighlight, Platform } from 'react-native'
import Swiper from 'react-native-swiper'
import Icon from 'react-native-vector-icons/FontAwesome'

import ProfileHeader from './ProfileHeader'
import ProfileInfo from './ProfileInfo'
import ProfileTicket from './ProfileTicket'

import { logoutAuth } from '../actions/auth'
import {
  getAllTicket,
  getTicket,
  deleteTicket
} from '../actions/ticket'
import {
  getUser,
  deleteUser
} from '../actions/user'
import { navigateTo } from '../actions/navigation'
import {
  SCREEN_TICKET_CREATION
} from '../constants/screen'

class ProfileScreen extends Component {
  static navigationOptions = {
    tabBarLabel: 'Profil',
    tabBarIcon: ({ tintColor }) => (
      <Icon name="user" size={25} color={ tintColor } />
    )
  }

  constructor(props) {
      super(props)
      this.state = {
        profileStep: 0,
        profile: null,
        profileTickets: []
      }

      this.setProfileStep = this.setProfileStep.bind(this)
      this.editProfile = this.editProfile.bind(this)
      this.logout = this.logout.bind(this)
  }

  componentDidMount() {
    const { user } = this.props.authentication
    this.props.getUser(user.username)
    this.props.getAllTicket(user.username)
  }

  // componentDidMount() {
  //   console.log('PROPS PROFILE')
  //   console.log(this.props.user_store.node)
  //   let profile = this.props.user_store.node
  //   // let profile_tickets = this.props.ticket_store
  //   console.log('PROPS PROFILE TICKET STORE')
  //   console.log(this.props.ticket_store)
  //   let self = this
  //   AsyncStorage.getItem('me').then((me) => {
  //     console.log('ME', me)
  //     let username = JSON.parse(me).username
  //     fetch('https://secure-sierra-71157.herokuapp.com/api/personnal_tickets', {
  //       method: 'post',
  //       headers: {
  //           'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
  //       },
  //       body: qs.stringify({
  //         username: username,
  //       })
  //     }).then(function (response) {
  //       return response.json()
  //     }).then(function (result) {
  //       console.log('RESULT FETCH TICKETS SUCEED')
  //       console.log(result)
  //       self.setState({ profileTickets: result })
  //     }).catch(function (error) {
  //       //[TODO] Display error span
  //       // console.log('Request login failed', error)
  //     })
  //   })
  //   // let profileTickets = this.props.ticket_store.tickets.edges
  //   // let profile = {
  //   //   avatarUrl: 'https://s-media-cache-ak0.pinimg.com/originals/aa/d8/b6/aad8b60d202b52f2f769b48ed59ff2c6.jpg',
  //   //   username: 'ImAtome',
  //   //   rank: 1,
  //   //   gender: 0,
  //   //   ticketsCount: 253,
  //   //   wins: 234,
  //   //   looses: 19,
  //   //   league: {
  //   //     id:0,
  //   //     name: 'Diamond'
  //   //   },
  //   //   tickets: [
  //   //     {
  //   //       matches: [
  //   //         {
  //   //           id:0,
  //   //           game: {
  //   //             id:0,
  //   //             name: 'CSGO',
  //   //             pictureUrl: require('../ressources/img/csgo_background.jpg')
  //   //           },
  //   //           teamA: {
  //   //             name: 'Manalight',
  //   //             logo: require('../ressources/img/manalight_logo.png'),
  //   //             vote: 213,
  //   //             selected: true
  //   //           },
  //   //           teamB: {
  //   //             name: 'Tricked Esport',
  //   //             logo: require('../ressources/img/tricked_logo.png'),
  //   //             vote: 56,
  //   //             selected: false
  //   //           }
  //   //         },
  //   //         {
  //   //           id:1,
  //   //           game: {
  //   //             id:1,
  //   //             name: 'LOL',
  //   //             pictureUrl: require('../ressources/img/lol_background.jpg')
  //   //           },
  //   //           teamA: {
  //   //             name: 'Team Curse',
  //   //             logo: require('../ressources/img/curse_logo.png'),
  //   //             vote: 4501,
  //   //             selected: false
  //   //           },
  //   //           teamB: {
  //   //             name: 'Tricked Esport',
  //   //             logo: require('../ressources/img/tricked_logo.png'),
  //   //             vote: 32456,
  //   //             selected: true
  //   //           }
  //   //         },
  //   //         {
  //   //           id:2,
  //   //           game: {
  //   //             id:2,
  //   //             name: 'OVERWATCH',
  //   //             pictureUrl: require('../ressources/img/overwatch_background.jpg')
  //   //           },
  //   //           teamA: {
  //   //             name: 'aaa',
  //   //             logo: require('../ressources/img/aaa_logo.png'),
  //   //             vote: 1345,
  //   //             selected: true
  //   //           },
  //   //           teamB: {
  //   //             name: 'Exes Esport',
  //   //             logo: require('../ressources/img/exes_logo.png'),
  //   //             vote: 564,
  //   //             selected: false
  //   //           }
  //   //         }
  //   //       ]
  //   //     },
  //   //   ]
  //   //}
  //
  //   this.setState({ profile: profile })
  // }

  setProfileStep(step) {
    if (step === 2) {
      // Edit profile
      return;
    }
    this.setState({ profileStep: step })
  }

  editProfile() {

  }

  logout() {
    // AsyncStorage.removeItem('me').then(() => {
    //   console.log('LOGOUT')
    //   this.props.navigator.push({ ident: "AuthenticationScreen" })
    // })
  }

  render() {
    console.log('PROPS TICKET STORE RENDER')
    console.log(this.props.ticket_store)
    const { tickets } = this.props.ticket
    const { user } = this.props.user
    console.log('USER IS PROFILE: ', user)
    return (
      <View style={ styles.profile }>
        <ProfileHeader
          username={ user ? user.username : 'Aimbet' }
          setProfileStep={ this.setProfileStep }
          profileStep={ this.state.profileStep } />
        {
          this.state.profileStep === 0 ? (
            <View style={ {flex:1} }>
              <ProfileInfo profile={ user } />
              <View style={ styles.profileTickets }>
                <View style={ styles.profileTicketsList }>
                  {
                    tickets.length > 0 ? (
                      <Swiper
                        style={ styles.profileTicketsListSwiper }
                        dot={
                          <View style={ {backgroundColor: 'rgba(255,255,255,.3)', width: 9, height: 9, borderRadius: 7, marginLeft: 3, marginRight: 3} }></View>
                        }
                        activeDot={
                          <View style={ {backgroundColor: '#fff', width: 9, height: 9, borderRadius: 7, marginLeft: 3, marginRight: 3} }></View>
                        }
                        paginationStyle={ {bottom:10} }
                        loop={ false }>
                        {
                          tickets.map((ticket, i) => {
                            console.warn('TICKET: ', ticket)
                            return (
                              <ProfileTicket key={ i } ticket={ ticket }/>
                            )
                          })
                        }
                      </Swiper>
                    ) : (
                      <View style={ {flex:1, width:400, justifyContent:'center', alignItems:'center', flexDirection:'column', padding:25} }>
                        <Text style={ {textAlign:'center', color:'white', fontWeight: 'bold'} }>
                          Aucun ticket à afficher.
                        </Text>
                        <TouchableHighlight
                          onPress={ () => this.props.navigateTo(SCREEN_TICKET_CREATION) }>
                          <Text style={ {textAlign:'center', color:'#00B04D', fontWeight: 'bold'} }>
                            Créer un ticket
                          </Text>
                        </TouchableHighlight>
                      </View>
                    )
                  }
                </View>
              </View>
            </View>
          ) : (
            <View style={ {flex:1, padding:25} }>
              <TouchableHighlight
                onPress={ () => this.props.logoutAuth() }>
                <Text style={ {textAlign:'center', color:'white', paddingTop: 10, paddingBottom:10, backgroundColor:'rgb(194, 21, 21)', fontWeight: 'bold'} }>
                  Se déconnecter
                </Text>
              </TouchableHighlight>
            </View>
          )
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({

  profile: {
    flex: 1,
    paddingTop:15,
    flexDirection: 'column',
    backgroundColor:'#0e1317'
  },

  profileTickets: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems:'flex-start'
  },

  profileTicketsList: {
    flex:1
  },

  profileTicketsListSwiper: {
  },

  profileTicketsListItemWrapper: {
    flex:1,
    flexDirection: 'column',
    justifyContent: 'center'
  },

  profileTicketsListItem: {
    flex:1,
    borderRadius:2,
    borderWidth:1,
    borderColor:'#00B04D',
    backgroundColor:'rgba(255,255,255, 0.3)',
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  }

})

function mapStateToProps(state) {
  return {
    ticket: state.ticket,
    user: state.user,
    authentication: state.authentication
  }
}

function mapDispatchToProps(dispatch) {
  return {
    navigateTo: (destination) => dispatch(navigateTo(destination)),
    logoutAuth: () => dispatch(logoutAuth()),
    getUser: (userId) => dispatch(getUser(userId)),
    removeUser: () => dispatch(removeUser()),
    getAllTicket: (username) => dispatch(getAllTicket(username)),
    removeTicket: (ticketId) => dispatch(removeTicket(ticketId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)
