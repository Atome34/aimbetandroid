'use strict'

import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableHighlight, Platform } from 'react-native'
import Swiper from 'react-native-swiper'
import Icon from 'react-native-vector-icons/FontAwesome'

class NotificationsScreen extends Component {
  constructor(props) {
      super(props)
      this.state = {
      }
  }

  render() {
    return (
      <View style={ styles.notifications }>
        <Text>NOTIFICATIONS</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({

  notifications: {
    flex: 1
  }

})

module.exports = NotificationsScreen
