import qs from 'qs'

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { AsyncStorage, StatusBar, StyleSheet, View, Text, TextInput, Image, TouchableHighlight } from 'react-native'
import Button from 'apsl-react-native-button'

import ViewContainer from './ViewContainer'
import StatusBarBackground from './StatusBarBackground'
import { loginAuth, resetAuth } from '../actions/auth'
import { navigateTo } from '../actions/navigation'
import {
  SCREEN_SIGNUP,
  SCREEN_SIGNUP_STEP_1
} from '../constants/screen'

class AuthenticationScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: ''
    }

    this.login = this.login.bind(this)
  }

  componentWillMount() {
      // console.warn('WillMount Fired')
  }

  componentDidMount() {
    // console.warn('didMount Fired')
  }

  login() {
    if (this.state.username && this.state.password)
      this.props.loginAuth({
        username: this.state.username,
        password: this.state.password,
      })
  }

  render() {
    const { loading, error } = this.props.authentication
    return (
      <ViewContainer style={ { backgroundColor: '#0e1317' } }>
        <StatusBar barStyle="light-content"/>
        <StatusBarBackground style={ { backgroundColor: '#0e1317' } }/>
        <View style={ styles.authenticationLogoWrapper }>
          <Image
            source={ require('../ressources/img/aimbet-logo_both-white.png') }
            style={ styles.authenticationLogo }/>
        </View>
        <View style={ styles.authenticationForm }>
          <TextInput
            style={ styles.authenticationInput }
            placeholder="Nom d'utilisateur"
            selectionColor="#00B04D"
            returnKeyType="next"
            autoCapitalize="none"
            autoCorrect={ false }
            maxLength={ 12 }
            onChangeText={ (username) => { this.setState({ username: username }) } }
            onSubmitEditing={ () => this.authenticationPasswordInput.focus() }
          />
          <TextInput
            style={ styles.authenticationInput }
            placeholder="Mot de passe"
            selectionColor="#00B04D"
            secureTextEntry={ true }
            returnKeyType="go"
            autoCapitalize="none"
            autoCorrect={ false }
            maxLength={ 30 }
            ref={ (input) => this.authenticationPasswordInput = input }
            onChangeText={ (password) => { this.setState({ password: password }) } }
            onSubmitEditing={ this.login }
          />
          {
            error ? (
              <Text style={ styles.authenticationTextError }>
                { error }
              </Text>
            ) : null
          }
          <Button
            textStyle={ { color:'white', fontWeight:'bold', fontSize:13 } }
            style={ styles.authenticationButton }
            disabledStyle={ { backgroundColor:'rgba(0, 176, 77, 0.5)', borderWidth:1, borderColor:"rgba(13, 126, 63, 0.8)" } }
            onPress={ this.login }
            isLoading={ loading }
            isDisabled={ !this.state.username || !this.state.password }>
            Se connecter
          </Button>
          <Text style={ styles.authenticationText }>
            Vous avez oublié vos identifiants de connexion ?
          </Text>
          <TouchableHighlight
            underlayColor='transparent'
            onPress={ () => { } }>
              <Text style={ styles.authenticationLink }>
                Obtenez de l'aide pour vous connecter.
              </Text>
          </TouchableHighlight>
          {/* <View style={ styles.authenticationOr }>
            <View style={ styles.authenticationLineWrapper }><View style={ styles.authenticationLine }></View></View>
            <View style={ styles.authenticationLineTextWrapper }><Text style={ styles.authenticationLineText }>OU</Text></View>
            <View style={ styles.authenticationLineWrapper }><View style={ styles.authenticationLine }></View></View>
          </View>
          <View style={ styles.authenticationTwitch }>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={ () => { console.log("Click on 'Twitch Connect Logo'") } }>
                  <Image
                    source={ require('../ressources/img/twitch-combo--white.png') }
                    style={ styles.authenticationTwitchLogo } />
            </TouchableHighlight>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={ () => { console.log("Click on 'Twitch Connect Text'") } }>
                  <Text style={ styles.authenticationTwitchText }>  Se connecter avec Twitch</Text>
            </TouchableHighlight>
          </View> */}
          <View style={ styles.authenticationSignUp }>
            <Text style={ styles.authenticationSignUpText }>Vous n'avez pas de compte ?</Text>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={ () => this.props.navigateTo(SCREEN_SIGNUP) }>
                <Text style={ styles.authenticationSignUpLink }> Inscrivez-vous.</Text>
            </TouchableHighlight>
          </View>
        </View>
      </ViewContainer>
    )
  }
}

const styles = StyleSheet.create({

  authenticationLogoWrapper: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20
  },

  authenticationLogo: {
    height: 100,
    resizeMode: 'contain'
  },

  authenticationForm: {
    flex: 4,
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 15,
    paddingBottom: 15
  },

  authenticationInput: {
    paddingLeft:10,
    paddingRight:10,
    height:40,
    backgroundColor:'white',
    borderRadius:2,
    borderWidth:1,
    borderColor:'#00B04D',
    marginBottom:15,
    fontSize:13
  },

  authenticationButton: {
    backgroundColor:'#00B04D',
    borderRadius:2,
    overflow:'hidden',
    alignItems:'center',
    height:40
  },

  authenticationText: {
    paddingTop:15,
    textAlign:'center',
    color: 'white',
    fontSize:13
  },

  authenticationTextError: {
    fontWeight:'bold',
    marginBottom: 15,
    paddingTop:5,
    paddingBottom:5,
    textAlign:'center',
    color: 'white',
    backgroundColor: 'rgb(173, 0, 0)',
    fontSize:13
  },

  authenticationLink: {
    color: '#00B04D',
    fontSize:13,
    fontWeight:'500',
    textAlign:'center'
  },

  authenticationOr: {
    paddingTop:40,
    flexDirection:'row',
    justifyContent:'center'
  },

  authenticationLineWrapper: {
    height:16,
    flexDirection:'column',
    justifyContent:'center',
    flex:1
  },

  authenticationLineTextWrapper: {
    width:80
  },

  authenticationLine: {
    height:0.4,
    backgroundColor:'rgb(83, 83, 83)'
  },

  authenticationLineText: {
    color:'grey',
    fontSize:12,
    textAlign:'center',
    fontWeight:'500'
  },

  authenticationTwitch: {
    flex:1,
    paddingTop:40,
    flexDirection:'column'
  },

  authenticationTwitchLogo: {
    width:65,
    height:40,
    alignSelf:'center'
  },

  authenticationTwitchText: {
    color:'white',
    fontWeight:'600',
    fontSize:13,
    paddingTop:10,
    textAlign:'center'
  },

  authenticationSignUp: {
    flexDirection: 'row',
    flex:1,
    alignItems:'flex-end',
    justifyContent:'center'
  },

  authenticationSignUpText: {
    fontSize:13,
    color: 'white'
  },

  authenticationSignUpLink: {
    fontWeight:'600',
    fontSize:13,
    color: '#00B04D'
  }

})

function mapStateToProps(state) {
  return {
    authentication: state.authentication
  }
}

function mapDispatchToProps(dispatch) {
  return {
    navigateTo: (destination) => dispatch(navigateTo(destination)),
    loginAuth: (data) => dispatch(loginAuth(data)),
    resetAuth: () => dispatch(resetAuth())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticationScreen)
