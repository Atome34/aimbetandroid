'use strict'

import React, { Component } from 'react'
import { StyleSheet, ListView, View, Text, Image, TouchableHighlight } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

const ProfileInfo = (props) => {
  console.log('PROFILE INFO PROPS')
  console.log(props)
  return (
    <View style={ styles.profileInfo }>
      <View style={ styles.profileInfoPictureWrapper }>
        {
          props.profile && props.profile.avatar ? (
            <Image
              source={ {uri: props.profile.avatar} }
              style={ styles.profileInfoPicture }/>
          ) : (
            <Image
              source={ require('../ressources/img/Aimbet_Logo_White.png') }
              style={ styles.profileInfoPicture }/>
          )
        }
      </View>
      <View style={ styles.profileInfoData }>
        <View style={ styles.profileInfoDataUser }>
          <View style={ styles.profileInfoDataUserRank }>
            <Text style={ styles.profileInfoText }>#{ props.profile && props.profile.rank }</Text>
          </View>
          <View style={ styles.profileInfoDataUserName }>
            <Icon name="square" size={13} color="#35A4FD" />
            <Text style={ styles.profileInfoText }>&nbsp;{ props.profile && props.profile.username }</Text>
          </View>
        </View>
        <View style={ styles.profileInfoDataStats }>
          <View style={ styles.profileInfoDataStatsCount}>
            <Icon name="ticket" size={15} color="white" />
            <Text style={ styles.profileInfoText }>&nbsp;{ props.profile && props.profile.tickets ? props.profile.tickets.length : 0 }</Text>
          </View>
          <View style={ styles.profileInfoDataStatsRatio}>
            <Icon name="line-chart" size={15} color="white" />
            <Text style={ styles.profileInfoText }>
              &nbsp;
              {
                props.profile && (props.profile.matchs_won !== 0 && props.profile.matchs_loose !== 0) ?
                  ((props.profile.matchs_won * 100) / (props.profile.matchs_won + props.profile.matchs_loose)).toFixed(1) : 0
              }%
            </Text>
          </View>
        </View>
        {/* <View style={ styles.profileInfoDataLeague }>
          <Icon name="diamond" size={15} color="#c2e6ff" />
          <Text style={ [styles.profileInfoText, { color: '#303030' }] }>&nbsp;{ props.profile.league ? `${props.profile.league.name} League`  : 'Alone' }</Text>
        </View> */}
      </View>
    </View>
  )

}

const styles = StyleSheet.create({

  profileInfo: {
    height: 100,
    padding:10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },

  profileInfoText: {
    fontWeight: 'bold',
    color: 'white',
  },

  profileInfoPictureWrapper: {
    width: 80,
    height: 80
  },

  profileInfoPicture: {
    resizeMode:'cover',
    backgroundColor:'transparent',
    width:80,
    height:80
  },

  profileInfoData: {
    flex:1,
    padding:5,
    backgroundColor:'rgba(255,255,255, 0.3)',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },

  profileInfoDataUser: {
    flex:1,
    marginBottom:5,
    flexDirection: 'row'
  },

  profileInfoDataUserRank: {
    alignSelf: 'stretch',
    justifyContent: 'center',
    marginRight:5,
    paddingLeft:10,
    paddingRight:10,
    backgroundColor: '#F8D11C'
  },

  profileInfoDataUserName: {
    flex:1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: 'rgba(255,255,255, 0.3)'
  },

  profileInfoDataUserNameIcon: {
  },

  profileInfoDataStats: {
    flex:1,
    flexDirection: 'row'
  },

  profileInfoDataStatsCount: {
    flex:1,
    marginRight:5,
    backgroundColor: '#00B04D',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

  },

  profileInfoDataStatsRatio: {
    flex:1,
    backgroundColor: '#F68D23',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },

  profileInfoDataLeague: {
    flex:1,
    backgroundColor: 'green',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'white'
  }

})

module.exports = ProfileInfo
