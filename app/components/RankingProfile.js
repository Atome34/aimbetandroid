'use strict'

import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableHighlight, Platform } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

const RankingProfile = (props) => {

  let colorRanking = (rank) => {
    if (rank === 1)
      return '#F8D11C'
    else if (rank === 2)
      return '#D6D6D6'
    else if (rank === 3)
      return '#D3622D'
    else
      return '#242424'
  }

  return (
    <TouchableHighlight
      style={ styles.rankingProfileWrapper }
      onPress={ () => {} }>
        <View style={ styles.rankingProfile }>
          <Image
            style={ styles.rankingProfilePicture }
            source={ require('../ressources/img/Aimbet_Logo_White.png') }/>
          <Text style={ [styles.rankingProfileText, { backgroundColor: colorRanking(props.profile ? props.profile.rank : 0) }] }>
            #{ props.profile && props.profile.rank }
          </Text>
          <Text style={ [styles.rankingProfileText, { backgroundColor: '#888888' }] }>
            <Icon name="square" size={11} color={ props.profile && props.profile.gender === 0 ? '#35A4FD' : '#E872DA' } />&nbsp;
            { props.profile && props.profile.username }
          </Text>
          <Text style={ [styles.rankingProfileText, { backgroundColor: '#00B04D' }] }>
            { props.profile && props.profile.matchs_won }
          </Text>
          <Text style={ [styles.rankingProfileText, { backgroundColor: '#D51515' }] }>
            { props.profile && props.profile.matchs_loose }
          </Text>
          <Text style={ [styles.rankingProfileText, { backgroundColor: '#F68D23' }] }>
            <Icon name="bar-chart" size={12} color='white' />&nbsp;
            {
              props.profile && (props.profile.matchs_won !== 0 && props.profile.matchs_loose !== 0) ?
                ((props.profile.matchs_won * 100) / (props.profile.matchs_won + props.profile.matchs_loose)).toFixed(1) : 0
            }%
          </Text>
        </View>
    </TouchableHighlight>
  )
}

const styles = StyleSheet.create({
  rankingProfileWrapper: {
    marginBottom:10,
    height:40
  },

  rankingProfile: {
    flex:1,
    paddingRight:10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems:'center',
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  },

  rankingProfilePicture: {
    width:40,
    height: 40,
    backgroundColor: 'white'
  },

  rankingProfileText: {
    paddingTop:5,
    paddingBottom:5,
    paddingLeft:10,
    paddingRight:10,
    color: 'white',
    fontSize:12,
    fontWeight: 'bold',
    textAlign:'center',
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  }

})

module.exports = RankingProfile
