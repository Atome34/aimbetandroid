import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  StatusBar,
  StyleSheet,
  View,
  Text,
  TextInput,
  Image,
  TouchableHighlight,
  Platform
} from 'react-native'
import Button from 'apsl-react-native-button'
import ImagePicker from 'react-native-image-picker'

import ViewContainer from './ViewContainer'
import StatusBarBackground from './StatusBarBackground'
import CreateUserMutation from '../mutations/SignUp'
import { navigateTo } from '../actions/navigation'
import { signUpAuth } from '../actions/auth'
import {
  SCREEN_SIGNIN,
  SCREEN_AUTHENTIFIED
} from '../constants/screen'

class SignUpStep2Screen extends Component {
  constructor(props) {
      super(props)
      this.state = {
        avatarUrl: null,
        username: null,
        password: null
      }

      this.selectAvatar = this.selectAvatar.bind(this)
      this.register = this.register.bind(this)
  }

  register() {
    const { email } = this.props.authentication
    if (this.state.username && this.state.password && email)
      this.props.signUpAuth({
        email,
        username: this.state.username,
        password: this.state.password
      })
  }

  selectAvatar() {
    const options = {
      title:"Ajouter votre avatar",
      takePhotoButtonTitle:"Prendre une photo",
      chooseFromLibraryButtonTitle:"Choisir une photo",
      cancelButtonTitle:"Retour"
    }
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source;

        // You can display the image using either data...
        source = { uri: 'data:image/jpeg;base64,' + response.data };

        // Or a reference to the platform specific asset location
        if (Platform.OS === 'android') {
          source = { uri: response.uri };
        } else {
          source = { uri: response.uri.replace('file://', '') };
        }

        this.setState({ avatarUrl: source });
      }
    });
  }

  render() {
    const { loading, error } = this.props.authentication
    return (
      <ViewContainer style={ { backgroundColor: '#0e1317' } }>
        <StatusBar barStyle="light-content"/>
        <StatusBarBackground style={ { backgroundColor: '#0e1317' } }/>
        <View style={ styles.authenticationForm }>
          <View style={ styles.authenticationInputFileButtonWrapper }>
            {/* <TouchableHighlight
              onPress={ this.selectAvatar }>
              <View style={ styles.authenticationInputFileButton }>
                { !this.state.avatarUrl ? (
                    <View>
                      <Text style={ [styles.authenticationInputFileButtonText, { fontSize:35 }] }>+</Text>
                      <Text style={ styles.authenticationInputFileButtonText }>Avatar</Text>
                    </View>
                  ) : (
                    <Image
                      source={ this.state.avatarUrl }
                      style={ styles.authenticationInputFileButtonImage }/>
                )}
              </View>
            </TouchableHighlight> */}
            <Image
              source={ require('../ressources/img/Aimbet_Logo_White.png') }
              style={ styles.authenticationInputFileButtonImage }/>
          </View>
          <TextInput
            style={ styles.authenticationInput }
            placeholder="Nom d'utilisateur"
            selectionColor="#00B04D"
            returnKeyType="next"
            autoCapitalize="none"
            autoCorrect={ false }
            maxLength={ 12 }
            onChangeText={ (username) => { this.setState({ username: username }) } }
            onSubmitEditing={ () => this.authenticationPasswordInput.focus() }
          />
          <TextInput
            style={ styles.authenticationInput }
            placeholder="Mot de passe"
            selectionColor="#00B04D"
            secureTextEntry={ true }
            returnKeyType="go"
            autoCapitalize="none"
            autoCorrect={ false }
            maxLength={ 30 }
            ref={ (input) => this.authenticationPasswordInput = input }
            onChangeText={ (password) => { this.setState({ password: password }) } }
            onSubmitEditing={ this.register }
          />
          {
            error ? (
              <Text style={ styles.authenticationTextError }>
                { error }
              </Text>
            ) : null
          }
          <Button
            textStyle={ { color:'white', fontWeight:'bold', fontSize:13 } }
            style={ styles.authenticationButton }
            disabledStyle={ { backgroundColor:'rgba(0, 176, 77, 0.5)', borderWidth:1, borderColor:"rgba(13, 126, 63, 0.8)" } }
            onPress={ this.register }
            isLoading={ loading }
            isDisabled={ !this.state.username || !this.state.password }>
            Créer votre compte
          </Button>
          <View style={ styles.authenticationSignUp }>
            <Text style={ styles.authenticationSignUpText }>
              Vous avez déjà un compte ?&nbsp;
            </Text>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={ () => this.props.navigateTo(SCREEN_SIGNIN) }>
                <Text style={ styles.authenticationSignUpLink }>
                  Connectez-vous.
                </Text>
            </TouchableHighlight>
          </View>
        </View>
      </ViewContainer>
    )
  }
}

const styles = StyleSheet.create({

  authenticationForm: {
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 15,
    paddingBottom: 10
  },

  authenticationInputFileButtonWrapper: {
    flexDirection:'row',
    justifyContent:'center',
    marginTop: 15,
    marginBottom: 15
  },

  authenticationInputFileButton: {
    backgroundColor:'rgba(255,255,255, 0.3)',
    width:100,
    height:100,
    flexDirection:'column',
    justifyContent:'center',
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  },

  authenticationInputFileButtonText: {
    color:'white',
    fontWeight:'600',
    textAlign:'center'
  },

  authenticationInputFileButtonImage: {
    resizeMode:'cover',
    width:100,
    height:100
  },

  authenticationFormLabel: {
    fontSize:17,
    color:'white',
    paddingTop:15,
    paddingBottom:10,
    fontWeight:'600'
  },

  authenticationInput: {
    paddingLeft:10,
    paddingRight:10,
    height:40,
    backgroundColor:'white',
    borderRadius:2,
    borderWidth:1,
    borderColor:'#00B04D',
    marginBottom:15,
    fontSize:13
  },

  authenticationButton: {
    backgroundColor:'#00B04D',
    borderRadius:2,
    overflow:'hidden',
    alignItems:'center',
    height:40
  },

  authenticationText: {
    paddingTop:15,
    textAlign:'center',
    color: 'white',
    fontSize:13
  },

  authenticationTextError: {
    fontWeight:'bold',
    marginBottom: 15,
    paddingTop:5,
    paddingBottom:5,
    textAlign:'center',
    color: 'white',
    backgroundColor: 'rgb(173, 0, 0)',
    fontSize:13
  },

  authenticationLink: {
    color: '#00B04D',
    fontSize:13,
    fontWeight:'500',
    textAlign:'center'
  },

  authenticationSignUp: {
    flex:1,
    flexDirection: 'row',
    alignItems:'flex-end',
    justifyContent:'center'
  },

  authenticationSignUpText: {
    fontSize:13,
    color: 'white'
  },

  authenticationSignUpLink: {
    fontWeight:'600',
    fontSize:13,
    color: '#00B04D'
  }

})

// export default Relay.createContainer(SignUpStep2Screen, {
//   fragments : {
//       user_store : () => Relay.QL `
//         fragment on UserStore {
// 	         ${CreateUserMutation.getFragment('user_store')}
//         }
//       `
//   }
// })

function mapStateToProps(state) {
  return {
    authentication: state.authentication
  }
}

function mapDispatchToProps(dispatch) {
  return {
    navigateTo: (destination) => dispatch(navigateTo(destination)),
    signUpAuth: (data) => dispatch(signUpAuth(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpStep2Screen)
