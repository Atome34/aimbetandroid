'use strict'

import React, { Component } from 'react'
import { connect } from 'react-redux'

import UnauthentifiedWithNavigationState from '../navigators/UnauthentifiedNavigator'

const UnauthentifiedScreens = (props) => {
  return <UnauthentifiedWithNavigationState/>
}

export default UnauthentifiedScreens
