'use strict'

import React, { Component } from 'react'

import { StyleSheet, View, Text, Image } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'


const LoadingScreen = (props) => {

  return (
    <View style={ styles.Loading }>
      <Icon style={ styles.LoadingIcon } name="spinner" size={ 30 } color={ 'white' } />
      <Text style={ styles.LoadingText }>{ props.message }</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  Loading: {
    flex:1,
    flexDirection:'column',
    justifyContent:'center',
    alignItems:'center'
  },

  LoadingIcon: {
    paddingBottom:15
  },

  LoadingText: {
    color:'white',
    fontSize:20,
    fontWeight:'bold'
  }
})

module.exports = LoadingScreen
