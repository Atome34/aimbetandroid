'use strict'

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { StatusBar, StyleSheet, View, Text, TextInput, Image, TouchableHighlight } from 'react-native'
import Button from 'react-native-button'

import ViewContainer from './ViewContainer'
import StatusBarBackground from './StatusBarBackground'
import { setEmailAuth } from '../actions/auth'
import { navigateTo } from '../actions/navigation'
import {
  SCREEN_SIGNIN,
  SCREEN_SIGNUP_STEP_2
} from '../constants/screen'

class SignUpStep1Screen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: null
    }

    this.handleNext = this.handleNext.bind(this)
  }

  componentDidMount() {
    this.setState({ email: this.props.authentication.email })
  }

  handleNext() {
    if (this.state.email) {
      this.props.setEmailAuth(this.state.email)
      this.props.navigateTo(SCREEN_SIGNUP_STEP_2)
    }
  }

  render() {
    return (
      <ViewContainer style={ { backgroundColor: '#0e1317' } }>
        <StatusBar barStyle="light-content"/>
        <StatusBarBackground style={ { backgroundColor: '#0e1317' } }/>
        <View style={ styles.authenticationForm }>
          <Text style={ styles.authenticationFormLabel }>
            Adresse e-mail
          </Text>
          <TextInput
            style={ styles.authenticationInput }
            placeholder="Adresse e-mail"
            defaultValue={ this.state.email }
            selectionColor="#00B04D"
            returnKeyType="go"
            autoCapitalize="none"
            autoCorrect={ false }
            keyboardType="email-address"
            onChangeText={ (email) => this.setState({ email: email }) }
            onSubmitEditing={ () => { this.state.email && this.props.navigateTo(SCREEN_SIGNUP_STEP_2) } }
          />
          <Button
            containerStyle={ { borderRadius:2, overflow:'hidden' } }
            style={ styles.authenticationButton }
            styleDisabled={ { color:'rgba(171, 217, 191, 0.85)', backgroundColor:'rgba(0, 176, 77, 0.5)', borderWidth:1, borderColor:"rgba(13, 126, 63, 0.8)" } }
            onPress={ this.handleNext }
            disabled={ this.state.email ? false : true }>
            Suivant
          </Button>
          <View style={ styles.authenticationSignUp }>
            <Text style={ styles.authenticationSignUpText }>Vous avez déjà un compte ?</Text>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={ () => { this.props.navigateTo(SCREEN_SIGNIN) } }>
                <Text style={ styles.authenticationSignUpLink }> Connectez-vous.</Text>
            </TouchableHighlight>
          </View>
        </View>
      </ViewContainer>
    )
  }
}

const styles = StyleSheet.create({

  authenticationForm: {
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 15,
    paddingBottom: 10
  },

  authenticationFormLabel: {
    fontSize:17,
    color:'white',
    paddingTop:15,
    paddingBottom:10,
    fontWeight:'600'
  },

  authenticationInput: {
    paddingLeft:10,
    paddingRight:10,
    height:40,
    backgroundColor:'white',
    borderRadius:2,
    borderWidth:1,
    borderColor:'#00B04D',
    marginBottom:15,
    fontSize:13
  },

  authenticationButton: {
    backgroundColor:'#00B04D',
    color:'white',
    height:40,
    fontSize:13,
    paddingTop:11
  },

  authenticationText: {
    paddingTop:15,
    textAlign:'center',
    color: 'white',
    fontSize:13
  },

  authenticationLink: {
    color: '#00B04D',
    fontSize:13,
    fontWeight:'500',
    textAlign:'center'
  },

  authenticationSignUp: {
    flex:1,
    flexDirection: 'row',
    alignItems:'flex-end',
    justifyContent:'center'
  },

  authenticationSignUpText: {
    fontSize:13,
    color: 'white'
  },

  authenticationSignUpLink: {
    fontWeight:'600',
    fontSize:13,
    color: '#00B04D'
  }

})

function mapStateToProps(state) {
  return {
    authentication: state.authentication
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setEmailAuth: (email) => dispatch(setEmailAuth(email)),
    navigateTo: (destination) => dispatch(navigateTo(destination))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpStep1Screen)
