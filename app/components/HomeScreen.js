'use strict'

import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableHighlight, Platform } from 'react-native'
import Swiper from 'react-native-swiper'
import Icon from 'react-native-vector-icons/FontAwesome'

class HomeScreen extends Component {
  constructor(props) {
      super(props)
      this.state = {
      }
  }

  render() {
    return (
      <View style={ styles.home }>
        <Text>HOME</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({

  home: {
    flex: 1
  }

})

module.exports = HomeScreen
