'use strict'

import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableHighlight, Platform } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

const CreateTicketHeader = (props) => {
    return (
      <View style={ styles.createTicketHeader }>
        {
          props.createTicketStep != 0 ? (
            <TouchableHighlight
              onPress={ () => props.setCreateTicketStep(props.createTicketStep - 1) }
              underlayColor='transparent'>
              <Icon name="chevron-left" size={18} color={ 'white' } />
            </TouchableHighlight>
          ) : <Icon name="chevron-left" size={18} color={ 'transparent' } />
        }
        {
          props.createTicketStep === 0 ? (
            <Text style={ styles.createTicketHeaderTitle }>Choix des jeux videos</Text>
          ) : props.createTicketStep === 1 ? (
            <Text style={ styles.createTicketHeaderTitle }>Selectionnez les vainqueurs</Text>
          ) : <Text style={ styles.createTicketHeaderTitle }>Votre ticket</Text>
        }
        {
          props.isNextStepReady ? (
            <TouchableHighlight
              onPress={ () => props.setCreateTicketStep(props.createTicketStep + 1) }
              underlayColor='transparent'>
              <Text style={ {color: 'white', fontWeight: 'bold'} }>OK</Text>
            </TouchableHighlight>
          ) : <Text style={ {color: 'transparent', fontWeight: 'bold'} }>OK</Text>
        }
      </View>
    )
}

const styles = StyleSheet.create({

  createTicketHeader: {
    height: 40,
    borderBottomWidth:0.5,
    borderColor: '#00B04D',
    backgroundColor: '#0e1317',
    paddingLeft:25,
    paddingRight:25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  createTicketHeaderTitle: {
    color: 'white',
    fontWeight: 'bold'
  }

})

module.exports = CreateTicketHeader
