'use strict'

import React, { Component } from 'react'
import { connect } from 'react-redux'

import SignUpWithNavigationState from '../navigators/SignUpNavigator'

const SignUpScreens = (props) => {
  return <SignUpWithNavigationState/>
}

export default SignUpScreens
