'use strict'

import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableHighlight } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import ViewContainer from './ViewContainer'
import StatusBarBackground from './StatusBarBackground'
import CreateTicketPickerWinner from './CreateTicketPickerWinner'

class CreateTicketMatch extends Component {
  constructor(props) {
      super(props)
      this.state = {}
  }

  render() {
    return (
      <CreateTicketPickerWinner
        teamsSelected= { this.props.teamsSelected }
        selectWinner={ this.props.selectWinner }
        removeWinner={ this.props.removeWinner }
        match={ this.props.match }/>
    )
  }
}

const styles = StyleSheet.create({

  profileTicketMatch: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    flex:1,
    width:null,
    height:null,
    marginBottom:10,
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  },

  profileTicketMatchWrapper: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    flex:1,
    backgroundColor: 'rgba(0 ,0, 0, 0.3)'
  },

  profileTicketMatchTeam: {
  },

  profileTicketMatchTeamInfo: {
    flexDirection: 'row',
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  },

  profileTicketMatchTeamInfoAvatar: {
    width:80,
    height:80,
    backgroundColor: 'white',
    resizeMode: 'contain'
  },

  profileTicketMatchTeamInfoStats: {
    height:80,
    paddingLeft:10,
    paddingRight:10,
    paddingTop:5,
    paddingBottom:5,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'space-between'
  },

  profileTicketMatchTeamInfoStatsText: {
      color:'white',
      fontWeight: 'bold',
      fontSize:12,
      textAlign: 'center'
  },

  profileTicketMatchTeamInfoStatsTextValue: {
    paddingLeft:5,
    paddingRight:5,
    paddingTop:3,
    paddingBottom:3
  },

  profileTicketMatchTeamName: {
    backgroundColor:'rgba(0, 0, 0, 0.5)',
    color:'white',
    padding:5,
    fontWeight: 'bold',
    marginTop:5,
    fontSize:12
  }

})

module.exports = CreateTicketMatch
