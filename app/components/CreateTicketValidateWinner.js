'use strict'

import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableHighlight } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import ViewContainer from './ViewContainer'
import StatusBarBackground from './StatusBarBackground'

class CreateTicketValidateWinner extends Component {

  constructor(props) {
    super(props)
    this.state = {}

    this.getColorVote = this.getColorVote.bind(this)
    this.getGameBackground = this.getGameBackground.bind(this)
    this.checkTeamSelected = this.checkTeamSelected.bind(this)
  }

  getColorVote(team) {
    let match = this.props.match.match
    if (match.team_a.vote > match.team_b.vote)
      return team === "TEAM_A" ? '#00B04D' : '#D51515'
    else if (match.team_a.vote < match.team_b.vote)
      return team === "TEAM_A" ? '#D51515' : '#00B04D'
    return 'black'
  }

  getGameBackground(name) {
    switch (name) {
      case 'CSGO':
        return require('../ressources/img/csgo_background.jpg')
      case 'OVERWATCH':
        return require('../ressources/img/overwatch_background.jpg')
      case 'LOL':
        return require('../ressources/img/lol_background.jpg')
      default:
        return null
    }
  }

  checkTeamSelected(idMatch, idTeam) {
    let teamsSelected = this.props.teamsSelected
    for (let i = 0; i < teamsSelected.length; ++i)
      if (teamsSelected[i].match.match_id == idMatch) {
        if (teamsSelected[i].winnerId == idTeam)
          return true
        else
          return false
      }
    return false
  }

  render() {
    let match = this.props.match.match
    return (
          <View style={ styles.profileTicketMatchPicker }>
            <TouchableHighlight
              style={ {zIndex:2} }
              onPress={ () => this.props.removeMatch(match.match_id) }>
              <View style={ styles.profileTicketMatchRemoveWrapper }>
                <Text style={ styles.profileTicketMatchRemove }>
                  <Icon name="times" size={15} color="white" />
                </Text>
              </View>
            </TouchableHighlight>
            <View style={ styles.profileTicketMatch  }>
              <Image
                style={ {
                flex:1,
                width:null,
                height:null,
                resizeMode:'cover'} }
                source={ this.getGameBackground('CSGO') }/>
              <View style={ styles.profileTicketMatchWrapper }>
                <View style={ styles.profileTicketMatchTeam }>
                  <View style={ [styles.profileTicketMatchTeamInfo, { justifyContent: 'flex-start' }] }>
                    <View style={ {width:80, height: 80, backgroundColor:'yellow'} }>
                      <Image
                        source={ {uri: match.team_a.logo} }
                        style={ styles.profileTicketMatchTeamInfoAvatar }/>
                      {
                        !this.checkTeamSelected(match.match_id, match.team_a.id) ? (
                          <View style={ styles.profileTicketMatchTeamInfoAvatarPickerWrapper }></View>
                        ) : null
                      }
                    </View>
                    <View style={ styles.profileTicketMatchTeamInfoStats }>
                      <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                        <Text style={ styles.profileTicketMatchTeamInfoStatsText }>VOTE</Text>
                        <Text
                          style={ [
                            styles.profileTicketMatchTeamInfoStatsText,
                            styles.profileTicketMatchTeamInfoStatsTextValue,
                            { backgroundColor: this.getColorVote('TEAM_A') }
                          ] }>{ match.team_a.vote }</Text>
                      </View>
                      <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                        <Text style={ styles.profileTicketMatchTeamInfoStatsText }>COTE</Text>
                        <Text style={ [
                          styles.profileTicketMatchTeamInfoStatsText,
                          styles.profileTicketMatchTeamInfoStatsTextValue,
                          { backgroundColor: '#F68D23'}
                        ] }>{ (match.team_a.vote * 100 / (match.team_a.vote + match.team_b.vote)).toFixed(1) }%</Text>
                      </View>
                    </View>
                  </View>
                  <View style={ {flexDirection: 'row'} }>
                    <Text style={ [styles.profileTicketMatchTeamName, { backgroundColor: this.checkTeamSelected(match.match_id, match.team_a.id) ? '#00B04D' : 'rgba(0, 0, 0, 0.5)' }] }>
                      { match.team_a.name.toUpperCase() }
                    </Text>
                  </View>
                </View>

                <View style={ styles.profileTicketMatchTeam }>
                  <View style={ [styles.profileTicketMatchTeamInfo, { justifyContent: 'flex-end' }] }>
                    <View style={ styles.profileTicketMatchTeamInfoStats }>
                      <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                        <Text style={ styles.profileTicketMatchTeamInfoStatsText }>VOTE</Text>
                        <Text style={ [
                          styles.profileTicketMatchTeamInfoStatsText,
                          styles.profileTicketMatchTeamInfoStatsTextValue,
                          { backgroundColor: this.getColorVote('TEAM_B') }
                        ] }>{ match.team_b.vote }</Text>
                      </View>
                      <View style={ styles.profileTicketMatchTeamInfoStatsItem }>
                        <Text style={ styles.profileTicketMatchTeamInfoStatsText }>COTE</Text>
                        <Text style={ [
                          styles.profileTicketMatchTeamInfoStatsText,
                          styles.profileTicketMatchTeamInfoStatsTextValue,
                          { backgroundColor: '#F68D23'}]
                        }>{ (match.team_b.vote * 100 / (match.team_a.vote + match.team_b.vote)).toFixed(1) }%</Text>
                      </View>
                    </View>
                    <View>
                      <Image
                        source={ {uri: match.team_b.logo} }
                        style={ styles.profileTicketMatchTeamInfoAvatar }/>
                        {
                          !this.checkTeamSelected(match.match_id, match.team_b.id) ? (
                            <View style={ styles.profileTicketMatchTeamInfoAvatarPickerWrapper }></View>
                          ) : null
                        }
                    </View>
                  </View>
                  <View style={ {flexDirection: 'row', justifyContent: 'flex-end'} }>
                    <Text style={ [styles.profileTicketMatchTeamName, { backgroundColor: this.checkTeamSelected(match.match_id, match.team_b.id) ? '#00B04D' : 'rgba(0, 0, 0, 0.5)' }] }>
                      { match.team_b.name.toUpperCase() }
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
    )
  }
}

const styles = StyleSheet.create({

  profileTicketMatch: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    position:'relative',
    flex:1,
    height:110,
    marginBottom:20,
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  },

  profileTicketMatchRemoveWrapper: {
    position:'absolute',
    alignItems:'center',
    justifyContent:'center',
    top:-10,
    right:-10,
    zIndex:1,
    width:20,
    height:20,
    backgroundColor: '#D51515'
  },

  profileTicketMatchRemove: {
    paddingBottom:2
  },

  profileTicketMatchPicker: {
    flex:1,
    flexDirection:'column',
    alignItems:'stretch'
  },

  profileTicketMatchPickerHeader: {
    paddingTop:3,
    paddingBottom:3,
    paddingLeft:10,
    paddingRight:10,
    textAlign: 'center',
    color:'black',
    fontWeight: 'bold',
    backgroundColor:'white'
  },

  profileTicketMatchWrapper: {
    justifyContent: 'space-between',
    position:'absolute',
    left:0,
    right:0,
    flexDirection: 'row',
    flex:1,
  },

  profileTicketMatchTeam: {
    height:110,
    backgroundColor:'transparent',
    justifyContent:'space-between'
  },

  profileTicketMatchTeamInfo: {
    flexDirection: 'row',
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
    }
  },

  profileTicketMatchTeamInfoAvatar: {
    width:80,
    height:80,
    backgroundColor: 'white',
    resizeMode: 'contain',
    alignItems: 'stretch'
  },

  profileTicketMatchTeamInfoAvatarPickerWrapper: {
    position:'absolute',
    top:0,
    width:80,
    height:80,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    justifyContent:'center',
    alignItems: 'center'
  },

  profileTicketMatchTeamInfoAvatarPicker: {
    width:35,
    height:35,
    borderWidth:3,
    borderColor: 'white',
    alignItems: 'center',
    justifyContent:'center'
  },

  profileTicketMatchTeamInfoStats: {
    height:80,
    paddingLeft:10,
    paddingRight:10,
    paddingTop:5,
    paddingBottom:5,
    backgroundColor:'rgba(0, 0, 0, 0.5)',
    justifyContent: 'space-between'
  },

  profileTicketMatchTeamInfoStatsText: {
      color:'white',
      fontWeight: 'bold',
      fontSize:12,
      textAlign: 'center'
  },

  profileTicketMatchTeamInfoStatsTextValue: {
    paddingLeft:5,
    paddingRight:5,
    paddingTop:3,
    paddingBottom:3
  },

  profileTicketMatchTeamName: {
    backgroundColor:'rgba(0, 0, 0, 0.5)',
    color:'white',
    padding:5,
    fontWeight: 'bold',
    marginTop:5,
    fontSize:12
  }

})

module.exports = CreateTicketValidateWinner
