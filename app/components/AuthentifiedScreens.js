'use strict'

import React, { Component } from 'react'
import Relay from 'react-relay'
import { AsyncStorage, StatusBar, StyleSheet, View, Text, Image, TouchableHighlight, Platform } from 'react-native'

import ViewContainer from './ViewContainer'
import StatusBarBackground from './StatusBarBackground'
import HomeScreen from './HomeScreen'
import RankingScreen from './RankingScreen'
import CreateTicketScreen from './CreateTicketScreen'
import NotificationsScreen from './NotificationsScreen'
import ProfileScreen from './ProfileScreen'
import LoadingScreen from './LoadingScreen'
import Navbar from './Navbar'

import RankingRoute from '../routes/Ranking'
import CreateTicketRoute from '../routes/TicketStore'
import ProfileRoute from '../routes/Profile'

class AuthentifiedScreens extends Component {
  constructor(props) {
      super(props)
      this.state = {
        navSelected: 'PROFILE',
        me: AsyncStorage.getItem('me').then((me) => {
          this.setState({ me: JSON.parse(me) })
        })
      }

      this.setNav = this.setNav.bind(this)
      this.displayScreen = this.displayScreen.bind(this)
  }

  // componentWillMount() {
  //   console.log('DID MOUNT')
  //   AsyncStorage.getItem('me').then((me) => {
  //     console.log('ME', me)
  //     this.setState({ me: JSON.parse(me) })
  //   })
  // }

  setNav(nav) {
    this.setState({ navSelected: nav })
  }

  displayScreen() {
    switch (this.state.navSelected) {
      case 'HOME':
        return <HomeScreen/>
      case 'RANKING':
        // console.log(RankingScreen)
        return (
          <Relay.Renderer
            Container={ RankingScreen }
            queryConfig={ new RankingRoute() }
            environment={ Relay.Store }
            render={({done, error, props, retry, stale}) => {
              if (props)
                return <RankingScreen { ...props }/>
              else
                return <LoadingScreen message="Loading..."/>
            }}/>
        )
      case 'CREATE_TICKET':
        // return (
        //   <Relay.Renderer
        //     Container={ CreateTicketScreen }
        //     queryConfig={ new CreateTicketRoute() }
        //     environment={ Relay.Store }
        //     render={({done, error, props, retry, stale}) => {
        //       if (props)
        //         return <CreateTicketScreen { ...props }/>
        //       else
        //         return <LoadingScreen message="Loading..."/>
        //     }}/>
        // )
        return <CreateTicketScreen setNav={ this.setNav }/>
      case 'NOTIFICATIONS':
        return <NotificationsScreen/>
      case 'PROFILE':
          return (
            <Relay.Renderer
              Container={ ProfileScreen }
              queryConfig={ new ProfileRoute({ username: this.state.me.username }) }
              environment={ Relay.Store }
              render={({done, error, props, retry, stale}) => {
                if (props) {
                  return <ProfileScreen setNav={ this.setNav } navigator={ this.props.navigator } { ...props }/>
                }
                else
                  return <LoadingScreen message="Loading..."/>
              }}/>
          )
        // return <ProfileScreen/>
      default:
        return
    }
  }

  render() {
    return (
      <ViewContainer style={ { backgroundColor: '#0e1317' } }>
        <StatusBar barStyle="light-content"/>
        <StatusBarBackground style={ { backgroundColor: '#0e1317' } }/>
        {
          this.displayScreen()
        }
        <Navbar setNav={ this.setNav } navSelected={ this.state.navSelected }/>
      </ViewContainer>
    )
  }
}

const styles = StyleSheet.create({
})

module.exports = AuthentifiedScreens
