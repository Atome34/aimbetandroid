import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  StyleSheet,
  View,
  AsyncStorage
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

import CreateTicketSelectGames from './CreateTicketSelectGames'
import CreateTicketSelectWinners from './CreateTicketSelectWinners'
import CreateTicketValidateTicket from './CreateTicketValidateTicket'

import { selectMatch, removeMatch } from '../actions/match'
import { createTicket } from '../actions/ticket'

class CreateTicketScreen extends Component {
  static navigationOptions = {
    tabBarLabel: 'Pronostiquer',
    tabBarIcon: ({ tintColor }) => (
      <Icon name="ticket" size={25} color={ tintColor } />
    )
  }

  constructor(props) {
      super(props)
      this.state = {
        createTicketStep: 0,
        videogamesSelected: []
      }

      this.setCreateTicketStep = this.setCreateTicketStep.bind(this)
      this.selectVideogame = this.selectVideogame.bind(this)
  }

  componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.match.matchesSelected.length === 0 && nextState.createTicketStep !== 0)
      this.setState({ createTicketStep: 0 })
  }

  setCreateTicketStep(step) {
    if (step === 3) {
      const { user } = this.props.auth
      console.warn('MATCHS SELECTED: ', this.props.match.matchesSelected)
      let winnersSelected = this.props.match.matchesSelected.map(team => ({
        match_id : team.match.match_id,
  	    team_a : team.match.team_a,
  	    team_b : team.match.team_b,
  	    date : team.match.date,
  	    tournament : team.match.tournament,
  	    prono_team : team.winnerId,
  	    comment : null
      }))
      console.warn('MATCHS SELECTED REPROG: ', winnersSelected)
      this.props.createTicket({username: user.username, matchs: winnersSelected})
      // AsyncStorage.getItem('me', (err, me) => {
      //   let username = 'Goku'
      //   fetch('https://secure-sierra-71157.herokuapp.com/api/create_ticket', {
      //     method: 'post',
      //     headers: {
      //         'Accept': 'application/json',
      //         'Content-Type': 'application/json'
      //     },
      //     body: JSON.stringify({
      //       username: username,
      //       matchs: winnersSelected
      //     })
      //   }).then(function (response) {
      //     return response.json()
      //   }).then(function (result) {
      //     console.warn('CREATE TICKET SUCCEED RESULT:', result)
      //     // self.props.setNav('PROFILE')
      //   }).catch (function (error) {
      //     //[TODO] Display error span
      //     console.log('Request Create Ticket failed', error)
      //   })
      // })
    }


      //Mutation Add ticket to profile
      // let winnersSelected = this.state.teamsSelected.map((team) => ({
      //   match_id : team.match.match_id,
  	  //   team_a : team.match.team_a,
  	  //   team_b : team.match.team_b,
  	  //   date : team.match.date,
  	  //   tournament : team.match.tournament,
  	  //   prono_team : team.winnerId,
  	  //   comment : null
      // }))
      // console.log('WINNERS SELECTED')
      // console.log(winnersSelected)
      // let self = this
      // AsyncStorage.getItem('me', (err, me) => {
      //   let username = JSON.parse(me).username
      //   fetch('https://secure-sierra-71157.herokuapp.com/api/create_ticket', {
      //     method: 'post',
      //     headers: {
      //         'Accept': 'application/json',
      //         'Content-Type': 'application/json'
      //     },
      //     body: JSON.stringify({
      //       username: username,
      //       matchs: winnersSelected
      //     })
      //   }).then(function (response) {
      //     return response.json()
      //   }).then(function (result) {
      //     console.log('CREATE TICKET SUCCEED')
      //     self.props.setNav('PROFILE')
      //   }).catch (function (error) {
      //     //[TODO] Display error span
      //     console.log('Request Create Ticket failed', error)
      //   })
      // })
      // return
    this.setState({ createTicketStep: step })
  }

  selectVideogame(videogameId) {
    let vgSelectedCopy = this.state.videogamesSelected.slice()
    let index = vgSelectedCopy.findIndex(id => id == videogameId)
    if (index !== -1)
      vgSelectedCopy.splice(index, 1)
    else
      vgSelectedCopy.push(videogameId)
    this.setState({ videogamesSelected: vgSelectedCopy })
  }

  render() {
    const { videogames } = this.props.videogame
    const { matches, matchesSelected } = this.props.match
    return (
      <View style={ styles.createTicket }>
        {
          this.state.createTicketStep === 0 ? (
            <CreateTicketSelectGames
              selectVideogame={ this.selectVideogame }
              videogames={ videogames }
              videogamesSelected={ this.state.videogamesSelected }
              setCreateTicketStep={ this.setCreateTicketStep }/>
          ) : (this.state.createTicketStep === 1 ? (
            <CreateTicketSelectWinners
              setCreateTicketStep={ this.setCreateTicketStep }
              teamsSelected={ this.props.match.matchesSelected }
              selectWinner={ this.props.selectMatch }
              matches={ matches }/>
          ) : (
            <CreateTicketValidateTicket
              setCreateTicketStep={ this.setCreateTicketStep }
              teamsSelected={ matchesSelected }
              removeMatch={ this.props.removeMatch }/>
          ))
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  createTicket: {
    flex: 1,
    backgroundColor:'#0e1317',
    paddingTop:15
  }
})

function mapStateToProps(state) {
  return {
    videogame: state.videogame,
    match: state.match,
    auth: state.authentication
  }
}

function mapDispatchToProps(dispatch) {
  return {
    selectMatch: (matchData) => dispatch(selectMatch(matchData)),
    removeMatch: (matchId) => dispatch(removeMatch(matchId)),
    createTicket: (data) => dispatch(createTicket(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTicketScreen)
