'use strict'

import React, { Component } from 'react'
import Relay from 'react-relay'
import { connect } from 'react-redux'
import {
  StyleSheet,
  View,
  ScrollView,
  ListView,
  Platform
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import RankingHeader from './RankingHeader'
import RankingProfile from './RankingProfile'

import {
  getUser,
  getAllUser
} from '../actions/user'

class RankingScreen extends Component {
  static navigationOptions = {
    tabBarLabel: 'Classement',
    tabBarIcon: ({ tintColor }) => (
      <Icon name="trophy" size={25} color={ tintColor } />
    )
  }

  constructor(props) {
      super(props)
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
      this.state = {
        rankingStep: 0,
        me: {},
        profiles: [],
        dataSource: ds.cloneWithRows([])
      }

      this.setRankingStep = this.setRankingStep.bind(this)
      this.displayProfileRow = this.displayProfileRow.bind(this)
  }

  componentDidMount() {
    const { user } = this.props.authentication
    this.props.getUser(user.username)
    this.props.getAllUser()
  }

  // componentDidMount() {
  //   // let me = this.props.user_store.users.edges.map((edge) => return edge.id).indexOf()
  //   let self = this
  //   AsyncStorage.getItem('me', (err, result) => {
  //     let me = JSON.parse(result)
  //     let data = self.props.user_store.users.edges.map((edge, i) => {
  //       edge.node['rank'] = i + 1
  //       return edge
  //     })
  //     data.sort((a, b) => {
  //       return a.points - b.points
  //     })
  //     console.log('RANKING ', data)
  //     this.setState({ me: me, profiles: data, dataSource: self.state.dataSource.cloneWithRows(data) })
  //   })
  //   // let me = {
  //   //   id: 0,
  //   //   pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //   name: 'ImAtome',
  //   //   gender: 0,
  //   //   rank: 1,
  //   //   wins: 234,
  //   //   looses: 19
  //   // }
  //   // let data = [
  //   //   {
  //   //     id: 0,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'ImAtome',
  //   //     gender: 0,
  //   //     rank: 1,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },
  //   //   {
  //   //     id: 1,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'Vegeta',
  //   //     gender: 0,
  //   //     rank: 2,
  //   //     wins: 17,
  //   //     looses: 10
  //   //   },
  //   //   {
  //   //     id: 2,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'Saïtama',
  //   //     gender: 0,
  //   //     rank: 3,
  //   //     wins: 16,
  //   //     looses: 11
  //   //   },
  //   //   {
  //   //     id: 3,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'IronMan',
  //   //     gender: 0,
  //   //     rank: 4,
  //   //     wins: 15,
  //   //     looses: 11
  //   //   },
  //   //   {
  //   //     id: 4,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'Batman',
  //   //     gender: 0,
  //   //     rank: 5,
  //   //     wins: 15,
  //   //     looses: 12
  //   //   },
  //   //   {
  //   //     id: 1,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'Vegeta',
  //   //     gender: 0,
  //   //     rank: 6,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },
  //   //   {
  //   //     id: 2,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'Saïtama',
  //   //     gender: 0,
  //   //     rank: 7,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },
  //   //   {
  //   //     id: 0,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'ImAtome',
  //   //     gender: 0,
  //   //     rank: 8,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },
  //   //   {
  //   //     id: 1,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'Vegeta',
  //   //     gender: 0,
  //   //     rank: 9,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },
  //   //   {
  //   //     id: 2,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'Saïtama',
  //   //     gender: 0,
  //   //     rank: 10,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },{
  //   //     id: 0,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'ImAtome',
  //   //     gender: 0,
  //   //     rank: 11,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },
  //   //   {
  //   //     id: 1,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'Vegeta',
  //   //     gender: 0,
  //   //     rank: 12,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },
  //   //   {
  //   //     id: 2,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'Saïtama',
  //   //     gender: 0,
  //   //     rank: 13,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },
  //   //   {
  //   //     id: 0,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'ImAtome',
  //   //     gender: 0,
  //   //     rank: 14,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },
  //   //   {
  //   //     id: 1,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'Vegeta',
  //   //     gender: 0,
  //   //     rank: 16,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },
  //   //   {
  //   //     id: 2,
  //   //     pictureUrl: require('../ressources/img/exes_logo.png'),
  //   //     name: 'Saïtama',
  //   //     gender: 0,
  //   //     rank: 14,
  //   //     wins: 234,
  //   //     looses: 19
  //   //   },
  //   // ]
  // }

  setRankingStep(step) {
    this.setState({ rankingStep: step })
  }

  displayProfileRow(rowData) {
    return <RankingProfile profile={ rowData } />
  }

  render() {
    const { user, users } = this.props.user
    return (
      <View style={ styles.ranking }>
        <RankingHeader
          setRankingStep={ this.props.setRankingStep }
          rankingStep={ this.state.rankingStep } />
        <RankingProfile profile={ user } />
        <View style={ {flex:1, paddingHorizontal:10} }>
          <ScrollView
            showsVerticalScrollIndicator={ false }>
            {
              users.map((user, index) => (
                <RankingProfile key={ index } profile={ user } />
              ))
            }
          </ScrollView>
        </View>
        {/* <ListView
          enableEmptySections
          style={ {marginTop:10, flex:1} }
          contentContainerStyle={ styles.rankingProfilesList }
          dataSource={ this.state.dataSource }
          renderRow={ (rowData) => this.displayProfileRow(rowData.node) } /> */}
      </View>
    )
  }
}

const styles = StyleSheet.create({

  ranking: {
    flex: 1,
    paddingTop:15,
    backgroundColor:'#0e1317'
  },

  rankingProfilesList: {
    backgroundColor:'red',
    flex:1,
    paddingHorizontal:10,
    paddingBottom:10
  }

})

function mapStateToProps(state) {
  return {
    authentication: state.authentication,
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getUser: (userId) => dispatch(getUser(userId)),
    getAllUser: () => dispatch(getAllUser())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RankingScreen)
