'use strict'

import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableHighlight, Platform } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

const RankingHeader = (props) => {
    return (
      <View style={ styles.rankingHeader }>
        {
          props.rankingStep !== 0 ? (
            <TouchableHighlight
              onPress={ () => props.setRankingStep(props.rankingStep - 1) }
              underlayColor='transparent'>
              <Icon name="chevron-left" size={18} color={ 'white' } />
            </TouchableHighlight>
          ) : <Icon name="chevron-left" size={18} color={ 'transparent' } />
        }
        {
          props.rankingStep === 0 ? (
            <Text style={ styles.rankingHeaderTitle }>Ranking</Text>
          ) : <Text style={ styles.rankingHeaderTitle }>{ props.name }</Text>
        }
        <Text style={ {color: 'transparent', fontWeight: 'bold'} }>...</Text>
      </View>
    )
}

const styles = StyleSheet.create({

  rankingHeader: {
    height: 40,
    borderBottomWidth:0.5,
    borderColor: '#00B04D',
    backgroundColor: '#0e1317',
    paddingLeft:25,
    paddingRight:25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  rankingHeaderTitle: {
    color: 'white',
    fontWeight: 'bold'
  }

})

module.exports = RankingHeader
