'use strict'

import React, { Component } from 'react'
import { connect } from 'react-redux'
import Relay from 'react-relay'
import {
  StyleSheet,
  View,
  Text,
  ListView,
  ScrollView,
  Image,
  TouchableHighlight,
  Platform
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'
import CreateTicketHeader from './CreateTicketHeader'
import CreateTicketMatch from './CreateTicketMatch'

import { getAllMatch } from '../actions/match'

class CreateTicketSelectWinners extends Component {
  constructor(props) {
      super(props)
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2 })
      this.state = {
        filterSelected: 'POPULAR',
        matchs: [],
        dataSource: ds.cloneWithRows([])
      }

      this.displayRow = this.displayRow.bind(this)
      this.setFilter = this.setFilter.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    // console.log('IN WILL RECEIVE PROPS')
    // console.log(nextProps)
    // if (nextProps) {
    //   let data = nextProps.match_store.matchs.edges.slice()
    //   data = data.map((match) => ({
    //     ...match,
    //     selected: this.findTeamSelected(nextProps.teamsSelected, match.node.match_id)
    //   }))
    //   console.log('DATA AFTER')
    //   console.log(data)
    //   this.setState({ dataSource: this.state.dataSource.cloneWithRows(data) })
    // }
  }

  findTeamSelected(teamsSelected, matchId) {
    let tS = teamsSelected
    for (let i = 0; i < tS.length; ++i)
      if (tS[i].match.match_id === matchId) {
        console.log('FIND MATCH ID TO SET SELECTED')
        return tS[i].winnerId
      }
    return null
  }

  componentDidMount() {
    // this.props.getAllMatch()
    // console.log('PROPS MATCH LIST')
    // console.log(this.props.match_store.matchs.edges)
    // let data = this.props.match_store.matchs.edges
    // let data = [
    //   {
    //     game: {
    //       id: 123,
    //       name: 'CSGO',
    //       background: '../ressources/img/csgo_background.jpg'
    //     },
    //     teamA: {
    //       id:0,
    //       name: 'aAa',
    //       logo: require('../ressources/img/aaa_logo.png'),
    //       vote: 1345,
    //       selected: true
    //     },
    //     teamB: {
    //       id:1,
    //       name: 'Exes Esport',
    //       logo: require('../ressources/img/exes_logo.png'),
    //       vote: 564,
    //       selected: false
    //     }
    //   },
    //   {
    //     game: {
    //       id: 124,
    //       name: 'OVERWATCH',
    //       background: '../ressources/img/overwatch_background.jpg'
    //     },
    //     teamA: {
    //       id: 2,
    //       name: 'Team Curse',
    //       logo: require('../ressources/img/curse_logo.png'),
    //       vote: 4501,
    //       selected: false
    //     },
    //     teamB: {
    //       id: 3,
    //       name: 'Fnatic',
    //       logo: require('../ressources/img/fnatic_logo.png'),
    //       vote: 32456,
    //       selected: true
    //     }
    //   },
    //   {
    //     game: {
    //       id: 125,
    //       name: 'LOL',
    //       background: '../ressources/img/lol_background.jpg'
    //     },
    //     teamA: {
    //       id: 4,
    //       name: 'Manalight',
    //       logo: require('../ressources/img/manalight_logo.png'),
    //       vote: 213,
    //       selected: true
    //     },
    //     teamB: {
    //       id: 5,
    //       name: 'Tricked Esport',
    //       logo: require('../ressources/img/tricked_logo.png'),
    //       vote: 56,
    //       selected: false
    //     }
    //   }
    // ]
    // this.setState({ dataSource: this.state.dataSource.cloneWithRows(data) })
  }

  displayRow(rowData, teamsSelected) {
    console.log('IN ROW')
    console.log(teamsSelected)
    return (
      <CreateTicketMatch
        selectWinner={ this.props.selectWinner }
        teamsSelected={ teamsSelected }
        match={ rowData.node }/>
    )
  }

  setFilter(filter) {
    this.setState({ filterSelected: filter })
  }

  // selectWinner(data) {
  //   let teamsSelected = this.state.teamsSelected.slice()
  //   for (let i = 0; i < teamsSelected.length; ++i)
  //     if (teamsSelected[i].idMatch == data.idMatch) {
  //       if (teamsSelected[i].idWinner == data.idWinner)
  //         teamsSelected.splice(i, 1)
  //       else
  //         teamsSelected[i].idWinner = data.idWinner
  //       this.setState({ teamsSelected: teamsSelected})
  //       return;
  //     }
  //   teamsSelected.push(data)
  //   this.setState({ teamsSelected: teamsSelected})
  // }

  render() {
    const matches = this.props.matches
    return (
      <View style={ styles.createTicket }>
        <CreateTicketHeader
          setCreateTicketStep={ this.props.setCreateTicketStep }
          createTicketStep={ 1 }
          isNextStepReady={ this.props.teamsSelected.length > 0 ? true : false }/>
        {/* <View style={ styles.createTicketSelectWinnersFilters }>
          <TouchableHighlight
            style={ [styles.createTicketSelectWinnersFilter, {borderColor: this.state.filterSelected === 'POPULAR' ? '#00B04D' : 'transparent'}] }
            onPress={ () => this.setFilter('POPULAR') }
            underlayColor='transparent'>
            <Text style={ styles.createTicketSelectWinnersFilterText }>POPULAR</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={ [styles.createTicketSelectWinnersFilter, {borderColor: this.state.filterSelected === 'RECENT' ? '#00B04D' : 'transparent'}] }
            onPress={ () => this.setFilter('RECENT') }
            underlayColor='transparent'>
            <Text style={ styles.createTicketSelectWinnersFilterText }>RECENT</Text>
          </TouchableHighlight>
        </View> */}
        <ScrollView
          showsVerticalScrollIndicator={ false }
          contentContainerStyle={ styles.createTicketMatchsList }>
          {
            matches.map((match, index) => (
              <CreateTicketMatch
                key={ index }
                selectWinner={ this.props.selectWinner }
                teamsSelected={ this.props.teamsSelected }
                match={ match }/>
            ))
          }
        </ScrollView>
        {/* <ListView
          contentContainerStyle={ styles.createTicketMatchsList }
          dataSource={this.state.dataSource}
          enableEmptySections
          renderRow={ (rowData) => this.displayRow(rowData, this.props.teamsSelected) } /> */}
      </View>
    )
  }
}

const styles = StyleSheet.create({

  createTicket: {
    flex: 1
  },

  createTicketSelectWinnersFilters: {
    height:60,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },

  createTicketSelectWinnersFilter: {
    padding:10,
    borderBottomWidth:2.5,
    borderColor:'transparent'
  },

  createTicketSelectWinnersFilterText: {
    textAlign:'center',
    marginLeft:5,
    marginRight:5,
    color:'white',
    fontWeight: 'bold'
  },

  createTicketMatchsList: {
    paddingHorizontal:10,
    paddingTop:10
  }

})

//
// export default Relay.createContainer(CreateTicketSelectWinners, {
//   fragments: {
//     match_store: () => Relay.QL `
//       fragment on MatchStore {
//         matchs(first: 3000) {
//           edges {
//             node {
//           		match_id,
//           		date,
//           		tournament,
//           		bg,
//           		comments,
//           		team_a,
//           		team_b
//             }
//           }
//         }
//       }
//     `
//   }
// })

function mapStateToProps(props) {
  return {}
}


function mapDispatchToProps(dispatch) {
  return {
    getAllMatch: () => dispatch(getAllMatch('CSGO'))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTicketSelectWinners)
