import { createStore, applyMiddleware, compose } from 'redux'
import { persistStore } from 'redux-persist'
import thunk from 'redux-thunk'

import rootReducer from './reducers'

function configureStore() {
  let store = createStore(
    rootReducer,
    undefined,
    compose(applyMiddleware(thunk))
  )
  let persistor = persistStore(store, null)
  return { persistor, store }
}

export default configureStore()
