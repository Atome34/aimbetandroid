import { NavigationActions } from 'react-navigation'

import {
  SCREEN_AUTHENTIFIED,
  SCREEN_UNAUTHENTIFIED
} from '../constants/screen'
import { RootAppNavigator } from '../navigators/RootAppNavigator'

const ActionForLoggedOut = RootAppNavigator.router.getActionForPathAndParams(SCREEN_UNAUTHENTIFIED)
const ActionForLoggedIn = RootAppNavigator.router.getActionForPathAndParams(SCREEN_AUTHENTIFIED)

const stateForLoggedOut = RootAppNavigator.router.getStateForAction(
  ActionForLoggedOut
)
const stateForLoggedIn = RootAppNavigator.router.getStateForAction(
  ActionForLoggedIn,
  stateForLoggedOut
)

const initialState = {
  stateForLoggedOut,
  stateForLoggedIn
}

export function rootNavigationReducer(state = initialState, action) {
  let nextState
  switch (action.type) {
    case SCREEN_AUTHENTIFIED:
      nextState = {
        ...state,
        stateForLoggedIn: RootAppNavigator.router.getStateForAction(
          ActionForLoggedIn,
          stateForLoggedOut
        )
      }
      break
    case SCREEN_UNAUTHENTIFIED:
      nextState = {
        ...state,
        stateForLoggedOut: NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.init({ routeName: SCREEN_UNAUTHENTIFIED })]
        })
      }
      break
    default:
      nextState = {
        ...state,
        stateForLoggedIn: RootAppNavigator.router.getStateForAction(
          action,
          stateForLoggedIn
        )
      }
      break
  }

  return nextState || state
}
