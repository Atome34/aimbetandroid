import { NavigationActions } from 'react-navigation'

import {
  SCREEN_RANKING,
  SCREEN_TICKET_CREATION,
  SCREEN_PROFILE
} from '../constants/screen'
import { AuthentifiedNavigator } from '../navigators/AuthentifiedNavigator'

const initialRootScreen = AuthentifiedNavigator.router.getActionForPathAndParams(SCREEN_PROFILE)
//
const initialAuthentifiedNavState = AuthentifiedNavigator.router.getStateForAction(
  initialRootScreen
)

export function authentifiedNavigationReducer(state = initialAuthentifiedNavState, action) {
  let nextState
  switch (action.type) {
    case SCREEN_RANKING:
      nextState = AuthentifiedNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: SCREEN_RANKING }),
        state
      )
      break
    case SCREEN_TICKET_CREATION:
      nextState = AuthentifiedNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: SCREEN_TICKET_CREATION }),
        state
      )
      break
    case SCREEN_PROFILE:
      nextState = AuthentifiedNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: SCREEN_PROFILE }),
        state
      )
      break
    default:
      nextState = AuthentifiedNavigator.router.getStateForAction(action, state)
      break
  }

  return nextState || state
}
