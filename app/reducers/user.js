import { NavigationActions } from 'react-navigation'

import {
  USER_GET,
  USER_GET_SUCCESS,
  USER_GET_FAILURE,
  USER_GETALL,
  USER_GETALL_SUCCESS,
  USER_GETALL_FAILURE,
  USER_DELETE,
  USER_DELETE_SUCCESS,
  USER_DELETE_FAILURE
} from '../constants/user'

const initialState = {
  user: {
      id: 0,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'ImAtome',
      gender: 0,
      rank: 1,
      matchs_won: 234,
      matchs_loose: 19
  },
  users: [
    {
      id: 0,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'ImAtome',
      gender: 0,
      rank: 1,
      matchs_won: 234,
      matchs_loose: 19
    },
    {
      id: 1,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'Vegeta',
      gender: 0,
      rank: 2,
      matchs_won: 17,
      matchs_loose: 10
    },
    {
      id: 2,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'Saïtama',
      gender: 0,
      rank: 3,
      matchs_won: 16,
      matchs_loose: 11
    },
    {
      id: 3,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'IronMan',
      gender: 0,
      rank: 4,
      matchs_won: 15,
      matchs_loose: 11
    },
    {
      id: 4,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'Batman',
      gender: 0,
      rank: 5,
      matchs_won: 15,
      matchs_loose: 12
    },
    {
      id: 1,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'Vegeta',
      gender: 0,
      rank: 6,
      matchs_won: 234,
      matchs_loose: 19
    },
    {
      id: 2,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'Saïtama',
      gender: 0,
      rank: 7,
      matchs_won: 234,
      matchs_loose: 19
    },
    {
      id: 0,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'ImAtome',
      gender: 0,
      rank: 8,
      matchs_won: 234,
      matchs_loose: 19
    },
    {
      id: 1,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'Vegeta',
      gender: 0,
      rank: 9,
      matchs_won: 234,
      matchs_loose: 19
    },
    {
      id: 2,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'Saïtama',
      gender: 0,
      rank: 10,
      matchs_won: 234,
      matchs_loose: 19
    },{
      id: 0,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'ImAtome',
      gender: 0,
      rank: 11,
      matchs_won: 234,
      matchs_loose: 19
    },
    {
      id: 1,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'Vegeta',
      gender: 0,
      rank: 12,
      matchs_won: 234,
      matchs_loose: 19
    },
    {
      id: 2,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'Saïtama',
      gender: 0,
      rank: 13,
      matchs_won: 234,
      matchs_loose: 19
    },
    {
      id: 0,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'ImAtome',
      gender: 0,
      rank: 14,
      matchs_won: 234,
      matchs_loose: 19
    },
    {
      id: 1,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'Vegeta',
      gender: 0,
      rank: 16,
      matchs_won: 234,
      matchs_loose: 19
    },
    {
      id: 2,
      pictureUrl: require('../ressources/img/exes_logo.png'),
      username: 'Saïtama',
      gender: 0,
      rank: 14,
      matchs_won: 234,
      matchs_loose: 19
    },
  ],
  loading: false,
  error: null
}
export function userReducer(state = initialState, action) {
  switch (action.type) {
    case USER_GET:
      return {
        ...state,
        loading: true
      }
    case USER_GET_SUCCESS:
      return {
        ...state,
        user: action.payload,
        loading: false,
        error: null
      }
    case USER_GET_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      }
      case USER_GETALL:
        return {
          ...state,
          loading: true
        }
      case USER_GETALL_SUCCESS:
        return {
          ...state,
          users: action.payload,
          loading: false,
          error: null
        }
      case USER_GETALL_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload
        }
      case USER_DELETE:
        return {
          ...state,
          loading: true
        }
      case  USER_DELETE_SUCCESS:
        return {
          ...state,
          user: null,
          loading: false,
          error: null
        }
      case  USER_DELETE_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload
        }
    default:
      return state
  }
}
