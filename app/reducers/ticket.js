import { NavigationActions } from 'react-navigation'

import {
  TICKET_CREATE,
  TICKET_CREATE_SUCCESS,
  TICKET_CREATE_FAILURE,
  TICKET_GET,
  TICKET_GET_SUCCESS,
  TICKET_GET_FAILURE,
  TICKET_GETALL,
  TICKET_GETALL_SUCCESS,
  TICKET_GETALL_FAILURE,
  TICKET_DELETE,
  TICKET_DELETE_SUCCESS,
  TICKET_DELETE_FAILURE
} from '../constants/ticket'

const initialState = {
  ticket: null,
  tickets: [],
  loading: false,
  error: null
}
export function ticketReducer(state = initialState, action) {
  switch (action.type) {
    case TICKET_CREATE:
      return {
        ...state,
        loading: true
      }
    case TICKET_CREATE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null
      }
    case TICKET_CREATE_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      }
      case TICKET_GET:
        return {
          ...state,
          loading: true
        }
      case TICKET_GET_SUCCESS:
        return {
          ...state,
          ticket: action.payload,
          loading: false,
          error: null
        }
      case TICKET_GET_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload
        }
      case TICKET_GETALL:
        return {
          ...state,
          loading: true
        }
      case TICKET_GETALL_SUCCESS:
        return {
          ...state,
          tickets: action.payload,
          loading: false,
          error: null
        }
      case TICKET_GETALL_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload
        }
      case TICKET_DELETE:
        return {
          ...state,
          loading: true
        }
      case TICKET_DELETE_SUCCESS:
        return {
          ...state,
          tickets: deleteTicket(state.tickets, action.payload),
          loading: false,
          error: null
        }
      case TICKET_DELETE_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload
        }
    default:
      return state
  }
}

function deleteTicket(tickets, ticketId) {
  let ticketsCopy = tickets.slice()
  let index = ticketsCopy.findIndex(ticket => ticket.id == ticketId)
  ticketsCopy.splice(index, 1)
  return ticketsCopy
}
