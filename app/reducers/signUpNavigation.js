import { NavigationActions } from 'react-navigation'

import {
  SCREEN_SIGNUP,
  SCREEN_SIGNUP_STEP_1,
  SCREEN_SIGNUP_STEP_2
} from '../constants/screen'
import { SignUpNavigator } from '../navigators/SignUpNavigator'

// const initialRootScreen = RootAppNavigator.router.getActionForPathAndParams('AuthenticationScreens')
//
const initialSignUpNavState = SignUpNavigator.router.getStateForAction(
  {}
)

export function signUpNavigationReducer(state = initialSignUpNavState, action) {
  let nextState
  switch (action.type) {
    case SCREEN_SIGNUP_STEP_1:
      nextState = SignUpNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: SCREEN_SIGNUP_STEP_1 }),
        state
      )
      break
    case SCREEN_SIGNUP_STEP_2:
      nextState = SignUpNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: SCREEN_SIGNUP_STEP_2 }),
        state
      )
      break
    default:
      nextState = SignUpNavigator.router.getStateForAction(action, state)
      break
  }

  return nextState || state
}
