import { NavigationActions } from 'react-navigation'

import {
  VIDEOGAME_GETALL,
  VIDEOGAME_GETALL_SUCCESS,
  VIDEOGAME_GETALL_FAILURE,
} from '../constants/match'

const initialState = {
  videogames:[
    {
      id: 0,
      logo: require('../ressources/img/csgo_logo.png'),
      name: 'CSGO'
    },
    {
      id: 1,
      logo: require('../ressources/img/sf5_logo.png'),
      name: 'Street Fighter'
    },
    {
      id:2,
      logo: require('../ressources/img/overwatch_logo.png'),
      name: 'Overwatch'
    },
    {
      id:3,
      logo: require('../ressources/img/lol_logo.png'),
      name: 'LoL'
    },
    {
      id:4,
      logo: require('../ressources/img/fifa17_logo.png'),
      name: 'Fifa 17'
    },
    {
      id:5,
      logo: require('../ressources/img/clash-royale_logo.png'),
      name: 'Clash Royale'
    }
  ],
  loading: false,
  error: null
}
export function videogameReducer(state = initialState, action) {
  switch (action.type) {
      case VIDEOGAME_GETALL:
        return {
          ...state,
          loading: true
        }
      case VIDEOGAME_GETALL_SUCCESS:
        return {
          ...state,
          videogames: action.payload,
          loading: false,
          error: null
        }
      case VIDEOGAME_GETALL_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload
        }
    default:
      return state
  }
}
