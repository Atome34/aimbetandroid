import { combineReducers } from 'redux'
import { createTransform, persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import { rootNavigationReducer } from './rootNavigation'
import { authentifiedNavigationReducer } from './authentifiedNavigation'
import { unauthentifiedNavigationReducer } from './unauthentifiedNavigation'
import { signUpNavigationReducer } from './signUpNavigation'
import { authenticationReducer } from './authentication'
import { userReducer } from './user'
import { ticketReducer } from './ticket'
import { matchReducer } from './match'
import { videogameReducer } from './videogame'

// let transforms = createTransform({
//   whitelist: [
//     'unauthentifiedNavigation',
//     'signUpNavigation'
//   ]
// })

const config = {
  key: 'root',
  storage,
  whitelist: [
    'authentication',
  ]
}

let rootReducer = persistCombineReducers(config, {
  rootNavigation: rootNavigationReducer,
  unauthentifiedNavigation: unauthentifiedNavigationReducer,
  authentifiedNavigation: authentifiedNavigationReducer,
  signUpNavigation: signUpNavigationReducer,
  authentication: authenticationReducer,
  user: userReducer,
  ticket: ticketReducer,
  match: matchReducer,
  videogame: videogameReducer
})

export default rootReducer
