import { NavigationActions } from 'react-navigation'

import {
  SCREEN_SIGNIN,
  SCREEN_SIGNUP
} from '../constants/screen'
import { UnauthentifiedNavigator } from '../navigators/UnauthentifiedNavigator'

const initialRootScreen = UnauthentifiedNavigator.router.getActionForPathAndParams(SCREEN_SIGNIN)
//
const initialUnauthentifiedNavState = UnauthentifiedNavigator.router.getStateForAction(
  initialRootScreen
)

export function unauthentifiedNavigationReducer(state = initialUnauthentifiedNavState, action) {
  let nextState
  switch (action.type) {
    case SCREEN_SIGNIN:
      nextState = UnauthentifiedNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: SCREEN_SIGNIN }),
        state
      )
      break
    case SCREEN_SIGNUP:
      nextState = UnauthentifiedNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: SCREEN_SIGNUP }),
        state
      )
      break
    default:
      nextState = UnauthentifiedNavigator.router.getStateForAction(action, state)
      break
  }

  return nextState || state
}
