import { NavigationActions } from 'react-navigation'

import {
  MATCH_GETALL,
  MATCH_GETALL_SUCCESS,
  MATCH_GETALL_FAILURE,
  MATCH_SELECT,
  MATCH_REMOVE
} from '../constants/match'

const initialState = {
  matchesSelected: [],
  matches:[
      {
          "_id": "5a17e26b734d1d3ed22dc854",
          "game": "CSGO",
          "url": "https://images.g2a.com/images/1024x768/1x1x0/b2d026bc88b7/590db26c5bafe391e14ed982",
          "match_id": 1,
          "date": "2017-12-24 11:00:00",
          "team_a": {
              "id":"234",
              "name": "AAA",
              "logo": "https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAe0AAAAJDRiOGU0YTEzLTFhMDAtNDg1Mi1iZTQ2LTY4OTdhN2NjOTdlOA.png",
              "vote": 24
          },
          "team_b": {
              "id":"235",
              "name": "Astralis",
              "logo": "https://pbs.twimg.com/profile_images/832222243614756864/GM50ie3U_400x400.jpg",
              "vote": 19
          },
          "winner": "",
          "closed": false,
          "tournament": "Fragbite Masters"
      },
      {
          "_id": "5a17e2f5734d1d3ed22dc897",
          "game": "CSGO",
          "url": "https://images.g2a.com/images/1024x768/1x1x0/b2d026bc88b7/590db26c5bafe391e14ed982",
          "match_id": 2,
          "date": "2017-12-23 15:00:00",
          "team_a": {
              "id":"420",
              "name": "Fnatic",
              "logo": "https://logos-download.com/wp-content/uploads/2016/06/Fnatic_logo_wordmark.png",
              "vote": 678
          },
          "team_b": {
              "id": "421",
              "name": "NIP",
              "logo": "https://ih1.redbubble.net/image.395403323.8739/flat,800x800,075,f.u1.jpg",
              "vote": 90
          },
          "winner": "",
          "closed": false,
          "tournament": "Fragbite Masters"
      },
      {
          "_id": "5a17e334734d1d3ed22dc8a6",
          "game": "CSGO",
          "url": "https://images.g2a.com/images/1024x768/1x1x0/b2d026bc88b7/590db26c5bafe391e14ed982",
          "match_id": 3,
          "date": "2017-12-23 22:00:00",
          "team_a": {
              "id": "520",
              "name": "SKT1",
              "logo": "https://ih0.redbubble.net/image.97194765.5620/flat,800x800,075,t.u5.jpg",
              "vote": 45
          },
          "team_b": {
              "id":"521",
              "name": "Samsung White",
              "logo": "https://ih1.redbubble.net/image.430524362.1647/flat,800x800,070,f.jpg",
              "vote": 8
          },
          "winner": "",
          "closed": false,
          "tournament": "Fragbite Masters"
      }
    ],
  loading: false,
  error: null
}
export function matchReducer(state = initialState, action) {
  switch (action.type) {
      case MATCH_GETALL:
        return {
          ...state,
          loading: true
        }
      case MATCH_GETALL_SUCCESS:
        return {
          ...state,
          matches: action.payload,
          loading: false,
          error: null
        }
      case MATCH_GETALL_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload
        }
      case MATCH_SELECT:
        return {
          ...state,
          matchesSelected: addMatch(state.matchesSelected, action.payload)
        }
      case MATCH_REMOVE:
        return {
          ...state,
          matchesSelected: removeMatch(state.matchesSelected, action.payload)
        }
    default:
      return state
  }
}

function addMatch(matchesSelected, matchData) {
  let matchesSelectedCopy = matchesSelected.slice()
  const matchIndex = matchesSelectedCopy.findIndex(matchSelected => matchSelected.match.match_id == matchData.match.match_id)
  if (matchIndex === -1)
    matchesSelectedCopy.push(matchData)
  else if (matchesSelectedCopy[matchIndex].winnerId == matchData.winnerId)
    matchesSelectedCopy.splice(matchIndex, 1)
  else
    matchesSelectedCopy[matchIndex].winnerId = matchData.winnerId
  return matchesSelectedCopy
}

function removeMatch(matchesSelected, matchId) {
  let matchesSelectedCopy = matchesSelected.slice()
  const matchIndex = matchesSelectedCopy.findIndex(matchSelected => matchSelected.match.match_id == matchId)
  if (matchIndex >= 0)
    matchesSelectedCopy.splice(matchIndex, 1)
  return matchesSelectedCopy
}
