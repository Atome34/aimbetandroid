import { NavigationActions } from 'react-navigation'

import {
  AUTH_LOGIN,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILURE,
  AUTH_SIGNUP,
  AUTH_SIGNUP_SUCCESS,
  AUTH_SIGNUP_FAILURE,
  AUTH_LOGOUT,
  AUTH_LOGOUT_SUCCESS,
  AUTH_LOGOUT_FAILURE,
  AUTH_SETEMAIL,
  AUTH_RESET
} from '../constants/auth'

const initialState = {
  user: null,
  email: null,
  loading: false,
  error: null
}
export function authenticationReducer(state = initialState, action) {
  switch (action.type) {
    case AUTH_LOGIN:
      return {
        ...state,
        loading: true
      }
    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        user: action.payload,
        loading: false,
        error: null
      }
    case AUTH_LOGIN_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      }
      case AUTH_SIGNUP:
        return {
          ...state,
          loading: true
        }
      case AUTH_SIGNUP_SUCCESS:
        return {
          ...state,
          user: action.payload,
          loading: false,
          error: null
        }
      case AUTH_SIGNUP_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload
        }
      case AUTH_LOGOUT:
        return {
          ...state,
          loading: true
        }
      case AUTH_LOGOUT_SUCCESS:
        return {
          ...state,
          user: null,
          loading: false,
          error: null
        }
      case AUTH_LOGOUT_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload
        }
      case AUTH_SETEMAIL:
        return {
          ...state,
          email: action.payload
        }
      case AUTH_RESET:
        return {
          ...state,
          error: null,
          loading: null
        }
    default:
      return state
  }
}
