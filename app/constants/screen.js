export const SCREEN_UNAUTHENTIFIED = 'SCREEN_UNAUTHENTIFIED'
export const SCREEN_AUTHENTIFIED = 'SCREEN_AUTHENTIFIED'

export const SCREEN_SIGNIN = 'SCREEN_SIGNIN'

export const SCREEN_SIGNUP = 'SCREEN_SIGNUP'
export const SCREEN_SIGNUP_STEP_1 = 'SCREEN_SIGNUP_STEP_1'
export const SCREEN_SIGNUP_STEP_2 = 'SCREEN_SIGNUP_STEP_2'

export const SCREEN_PROFILE = 'SCREEN_PROFILE'

export const SCREEN_TICKET_CREATION = 'SCREEN_TICKET_CREATION'

export const SCREEN_RANKING = 'SCREEN_RANKING'
