import Relay from 'react-relay'

export default class LoginMutation extends Relay.Mutation {
    static fragments = {
	     account_store : () => Relay.QL`
  	      fragment on AccountStore {
  	         id
  	      }
        `
    }

    getMutation() {
	     return Relay.QL `
  	      mutation {
  	         login
  	      }
       `
    }

    getVariables = () => {
    	return {
    	    username : this.props.username,
    	    password : this.props.password
    	}
    }

    getFatQuery() {
	     return Relay.QL `
	      fragment on LoginPayload {
	         clientMutationId
	      }
	     `
    }

    getConfigs() {
    	return [{
    	    type : 'RANGE_ADD',
    	    parentName : 'account_store',
    	    parentID: this.props.account_store.id,
    	    connectionName: 'accounts',
    	    edgeName: 'new_account_edge',
    	    rangeBehaviors: {'' : 'append'}
    	}]
    }
}
