// import Relay from 'react-relay'
//
// export default class CreateUserMutation extends Relay.Mutation {
//     static fragments = {
// 	     user_store : () => Relay.QL`
//       	fragment on UserStore {
//       	    id
//       	}
//       	`
//     }
//
//     getMutation() {
// 	   return Relay.QL `
//       	mutation {
//       	   create_user
//       	}
//       `
//     }
//
//     getVariables = () => {
// 	    /*ADD BIRTHDATE TO FORM*/
//     	console.log({
//     	    username : this.props.username,
//     	    email : this.props.email,
//     	    password : this.props.password,
//     	    birthdate : this.props.birthdate
//     	})
//     	return {
//     	    username : this.props.username,
//     	    email : this.props.email,
//     	    password : this.props.password,
//     	    birthdate : this.props.birthdate
//     	}
//     }
//
//     getFatQuery() {
//     	return Relay.QL `
//       	fragment on CreateUserPayload {
//       	    clientMutationId
//       	}
//     	`
//     }
//
//     getConfigs() {
//       console.log(this.props.user_store)
//     	return [{
//     	    type : 'RANGE_ADD',
//     	    parentName : 'user_store',
//     	    parentID: this.props.user_store.id,
//     	    connectionName: 'users',
//     	    edgeName: 'new_user_edge',
//     	    rangeBehaviors: {'' : 'append'}
//     	}]
//     }
// }
