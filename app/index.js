import React, { Component } from 'react'
import {
  View,
  StatusBar
} from 'react-native'
import { connect } from 'react-redux'

import RootAppWithNavigationState from './navigators/RootAppNavigator'
import {
  SCREEN_AUTHENTIFIED
} from './constants/screen'
import { navigateTo } from './actions/navigation'

// import AuthenticationScreens from './components/AuthenticationScreens'
// import AuthenticatedScreens from './components/AuthenticatedScreens'


const App = (props) => {
  return (
    <View style={ {flex: 1} }>
      <RootAppWithNavigationState />
    </View>
  )
}

function mapStateToProps(state) {
  return {
    authentication: state.authentication
  }
}

function mapDispatchToProps(dispatch) {
  return {
    navigateTo: (destination) => dispatch(navigateTo(destination))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
